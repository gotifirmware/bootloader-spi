#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include <string>

//#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#include "aardvark.h"
#include "SPIMaster.h" 

// Typedef of the callback
typedef void(*read_callback)(const uint8_t* data);

static const char help_str[] = "Support command list:\n\
'help'				// print command list\n\
'all'				// Show all posssible information\n\
'update'			// Update firmware\n\
'blversion'			// Bootloader Revision(0 if no bootloader)\n\
'version'			// Application Revision\n\
'satus'				// Module Status\n\
'control'			// Module Control\n\
'key1'				// Keypad One Status\n\
'key2'				// Keypad Two Status\n\
'wheellast'			// Wheel (Swipe Pad) Last(i.e., most recent) Position\n\
'wheelprev'			// Wheel (Swipe Pad) Previous Position\n\
'guard'				// 5.1.9.	Guard Key Data; Address: 16 � 17 (0x10 � 0x11)\n\
'counter'			// Free Running Counter(10 ms period)\n\
'led'				// LED Register\n\
'ledblink'			// LED Blink Register\n\
'blinkrate'			// LED Blink Rate(10 ms units)\n\
'wheel0'			// Wheel Sector 0 Output\n\
'wheel1'			// Wheel Sector 1 Output\n\
'wheel2'			// Wheel Sector 2 Output\n\
'signature'			// Signature byte for the CSIM; always reads 0x47\n\
---\n\
*tip:  add deciaml value after command to write into register";


//=========================================================================
// CONSTANTS
//=========================================================================
#define BUFFER_SIZE  2048
#define SPI_BITRATE  100
#define SPI_MODE     (AA_SPI_POL_RISING_FALLING | AA_SPI_PHASE_SAMPLE_SETUP)

//=========================================================================
// VARIABLES
//=========================================================================
Aardvark	handle;

static uint8_t io_buf[IO_BUFFER_SIZE_MAX];
static read_callback master_read_callback;

//=========================================================================
// FUNCTIONS
//=========================================================================

//=========================================================================
// Find USB I2C/SPI convertor
//=========================================================================
static int SPIMaster_Find_Aadvark(void)
{
	u16 ports[16];
	u32 unique_ids[16];
	int nelem = 16;

	// Find all the attached devices
	int count = aa_find_devices_ext(nelem, ports, nelem, unique_ids);
	int i;

	printf("%d device(s) found:\n", count);

	// Print the information on each device
	if (count > nelem)  count = nelem;
	for (i = 0; i < count; ++i) 
	{
		// Determine if the device is in-use
		const char *status = "(avail) ";
		if (ports[i] & AA_PORT_NOT_FREE) 
		{
			ports[i] &= ~AA_PORT_NOT_FREE;
			status = "(in-use)";
		}

		// Display device port number, in-use status, and serial number
		printf("    port=%-3d %s (%04d-%06d)\n",
			ports[i], status,
			unique_ids[i] / 1000000,
			unique_ids[i] % 1000000);

		return ports[i];
	}
	
	return -1;
}


//=========================================================================
// Init SPI master module
//=========================================================================
int SPIMaster_Init(void(*f_ptr)(const uint8_t* SPIMaster_ReadData))
{
	int	port = 0;
	int	mode = 0;
	int bitrate = 0;

	// TRy to find connected adpater
	port = SPIMaster_Find_Aadvark();
	if (port < 0) 
	{
		printf("No Aardvark device found!\n");
		printf("Error code = %d\n", port);

		system("PAUSE");
		return PROT_COMM_Error;
	}	
	
	// Open the device
	handle = aa_open(port);
	if (handle <= 0) 
	{
		printf("Unable to open Aardvark device on port %d\n", port);
		printf("Error code = %d\n", handle);

		system("PAUSE");
		return PROT_COMM_Error;
	}

	// Ensure that the SPI subsystem is enabled
	aa_configure(handle, AA_CONFIG_SPI_I2C);

	// Enable the Aardvark adapter's power pins.
	// This command is only effective on v2.0 hardware or greater.
	// The power pins on the v1.02 hardware are not enabled by default.
	aa_target_power(handle, AA_TARGET_POWER_BOTH);

	// Setup the clock phase
	// polarity	AA_SPI_POL_RISING_FALLING or AA_SPI_POL_FALLING_RISING
	// phase	AA_SPI_PHASE_SAMPLE_SETUP or AA_SPI_PHASE_SETUP_SAMPLE
	// bitorder	AA_SPI_BITORDER_MSB or AA_SPI_BITORDER_LSB	
	mode = SPI_MODE;
	aa_spi_configure(handle, (AardvarkSpiPolarity)(mode >> 1), (AardvarkSpiPhase)(mode & 1), AA_SPI_BITORDER_MSB);

	// Setup the bitrate
	bitrate = aa_spi_bitrate(handle, SPI_BITRATE);
	printf("Bitrate set to %d kHz\n", bitrate);
		
	master_read_callback = f_ptr;

	return PROT_COMM_Success;
}


//=========================================================================
// Close the device
//=========================================================================
void SPIMaster_Close(void)
{
	aa_close(handle);
}

//=========================================================================
// Main convertor API transive function for bootloader protocol
//=========================================================================
uint16_t SPIMaster_TransiveBoot(const uint8_t* wr_buffer, uint8_t wr_length, uint8_t* rd_buffer, uint8_t rd_length)
{
//	DWORD dwBytesWritten;
//	DWORD iSize;
//	uint8_t buff_in[4];
	int count;	

	// Write the data to the bus
	count = aa_spi_write(handle, (u16)(wr_length + rd_length), wr_buffer, (u16)(wr_length + rd_length), io_buf);
	if (count < 0) 
	{
		printf("error: %s\n", aa_status_string(count));
		return 0;
	}
	else if (count != (wr_length + rd_length)) 
	{
		printf("error: only a partial number of bytes written\n");
		printf("  (%d) instead of full (%d)\n", count, wr_length);
	}

	// Sleep a tad to make sure slave has time to process this request
	aa_sleep_ms(10);

	// Copy real data
	memcpy(rd_buffer, &io_buf[wr_length], rd_length);
	
	// If Read operation
	return (uint16_t)rd_length;
}
//=========================================================================
// Main convertor API transive function for Application protocol
//=========================================================================
uint16_t SPIMaster_TransiveApp(const uint8_t* wr_buffer, uint8_t* rd_buffer, uint8_t length)
{
	int count;

	// Write the data to the bus
	count = aa_spi_write(handle, (u16)(length), wr_buffer, (u16)(length), rd_buffer);
	if (count < 0)
	{
		printf("error: %s\n", aa_status_string(count));
		return 0;
	}
	else if (count != (length))
	{
		printf("error: only a partial number of bytes written\n");
		printf("  (%d) instead of full (%d)\n", count, length);
	}

	// Sleep a tad to make sure slave has time to process this request
	aa_sleep_ms(10);

	// If Read operation
	return (uint16_t)length;
}


//=========================================================================
// Return status reg value (ready/busy flag)
//=========================================================================
static uint8_t SPIMaster_ReadStatusReg(void)
{	
	uint8_t buff_in[1] = { BL_CMD_STATUSR };
	uint8_t buff_out[1];

	SPIMaster_TransiveBoot(buff_in, 1, buff_out, 1);
	
	return  buff_out[0];
}

//=========================================================================
// Check current state (0-app runnibg 1-bootloader)
//=========================================================================
static uint8_t SPIMaster_IsBootloader(void)
{
	uint8_t buff_in[1] = { BL_CMD_ISBOOTLOADER };
	uint8_t buff_out[1];

	SPIMaster_TransiveBoot(buff_in, 1, buff_out, 1);

	return  (buff_out[0] == 0xBB); 
}

//=========================================================================
// Return App/Bootloader 1-byte version
//=========================================================================
static uint8_t SPIMaster_GetVersion(void)
{
	uint8_t buff_in[1] = { BL_CMD_GET_VERSION };
	uint8_t buff_out[1];

	SPIMaster_TransiveBoot(buff_in, 1, buff_out, 1);

	return  buff_out[0];
}


//=========================================================================
// Reset Slave
//=========================================================================
void SPIMaster_ResetMCU(void)
{
	uint8_t buff_in[2] = { BL_CMD_RESET, BL_CMD_RESET };
	SPIMaster_TransiveBoot(buff_in, 2, NULL, 0);
}

//=========================================================================
// Set New update ready flag
//=========================================================================
void SPIMaster_SetUpdateReadyFlag(void)
{
	uint8_t buff_in[2] = { BL_CMD_SET_UPDRDY_FLAG, BL_CMD_SET_UPDRDY_FLAG };
	SPIMaster_TransiveBoot(buff_in, 2, NULL, 0);
}

//=========================================================================
// Clear update ready flag
//=========================================================================
void SPIMaster_ClearUpdateReadyFlag(void)
{
	uint8_t buff_in[2] = { BL_CMD_CLR_UPDRDY_FLAG, BL_CMD_CLR_UPDRDY_FLAG };
	SPIMaster_TransiveBoot(buff_in, 2, NULL, 0);
}

//=========================================================================
// Write data command
//=========================================================================
void SPIMaster_WriteData(uint32_t addr, const uint8_t* data, uint16_t length)
{
	// Write data
	io_buf[0] = BL_CMD_WRITE;
	io_buf[1] = ((addr >> 24) & 0xFF);
	io_buf[2] = ((addr >> 16) & 0xFF);
	io_buf[3] = ((addr >> 8) & 0xFF);
	io_buf[4] = ((addr >> 0) & 0xFF);
	memcpy(io_buf + 5, data, length);

	SPIMaster_TransiveBoot(io_buf, length + 5, NULL, 0);
}

//=========================================================================
// Read data back command
//=========================================================================
void SPIMaster_ReadData(uint32_t addr, uint8_t* data, uint32_t length)
{
	uint8_t buff_in[5];

	// Read data
	buff_in[0] = BL_CMD_READ;
	buff_in[1] = ((addr >> 24) & 0xFF);
	buff_in[2] = ((addr >> 16) & 0xFF);
	buff_in[3] = ((addr >> 8) & 0xFF);
	buff_in[4] = ((addr >> 0) & 0xFF);	

	SPIMaster_TransiveBoot(buff_in, 5, data, length);
}

//=========================================================================
// Show progress bar
//=========================================================================
void SPIMaster_PogressBar(uint16_t val, uint16_t max)
{
	uint16_t proc;

	// Output progress bar
	proc = (val + 1) * 100 / max;
	if (proc > 100) proc = 100;
	printf("[");
	for (uint8_t k = 1; k <= 25; k++) 
	{
		if (k <= proc / 4) 
		{
			printf("#");
		}
		else 
		{
			printf(" ");
		}
	}

	// Output progress bar
	printf("] %i %%\r", int(proc));	
}

//=========================================================================
// SPI transive with pauses
//=========================================================================
static void SPI_TransiveApp(uint8_t *buff_in, uint8_t *buff_out, uint8_t len)
{
	uint8_t i;

	for (i = 0; i < len; i++)
	{
		SPIMaster_TransiveApp(&buff_in[i], &buff_out[i], 1);		
		Sleep(10); 
	}
}


//=========================================================================
// Read Customer protocol registers
//=========================================================================
static uint8_t App_ReadRegisters(int cmd)
{
	uint8_t buff_in[7];
	uint8_t buff_out[7];

	// Bootloader Revision
	if (cmd == CMD_NUM_BL_REVISION || cmd == CMD_NUM_ALL)
	{
		printf("-Bootloader Revision-\n");		
		buff_in[0] = 0x80; buff_in[1] = 0x81; buff_in[2] = 0x82; buff_in[3] = 0x83; buff_in[4] = 0x84; buff_in[5] = SPI_DUMMY_BYTE; buff_in[6] = SPI_RD_SIGNATURE;
		SPI_TransiveApp(buff_in, buff_out, 7);			
		printf("0x%02X 0x%02X 0x%02X 0x%02X 0x%02X\n", buff_out[2], buff_out[3], buff_out[4], buff_out[5], buff_out[6]);
	}

	// Application Revision
	if (cmd == CMD_NUM_APP_REVISION || cmd == CMD_NUM_ALL)
	{
		printf("-Application Revision-\n");		
		buff_in[0] = 0x85; buff_in[1] = 0x86; buff_in[2] = 0x87; buff_in[3] = 0x88; buff_in[4] = 0x89; buff_in[5] = SPI_DUMMY_BYTE; buff_in[6] = SPI_RD_SIGNATURE;
		SPI_TransiveApp(buff_in, buff_out, 7);		
		printf("0x%02X 0x%02X 0x%02X 0x%02X 0x%02X\n", buff_out[2], buff_out[3], buff_out[4], buff_out[5], buff_out[6]);
	}

	///FAIL Module Status
	if (cmd == CMD_NUM_STATUS || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_STATUS; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-Module Status-\n");
		printf("[0x%02X]\n", buff_out[2]);
		printf("Run State: ");
		if (buff_out[2] & 0x80)	 printf("Bootloader\n");
		else printf("Application\n");
		printf("Reset Status: ");
		if (buff_out[2] & 0x10)	 printf("Module Reset\n");
		else printf("Reset Cleared\n");
		printf("IRQ Status: ");
		if (buff_out[2] & 0x08)	 printf("CSIM IRQ Asserted\n");
		else printf("No Change\n");
		printf("Key Status Changed: ");
		if (buff_out[2] & 0x04)	 printf("Changed\n");
		else printf("No Change\n");
		printf("Swipe Pad Status Changed: ");
		if (buff_out[2] & 0x01)	 printf("Changed\n");
		else printf("No Change\n");
	}

	// Module Control 
	if (cmd == CMD_NUM_CONTROL || cmd == CMD_NUM_ALL)
	{		
		buff_in[0] = 0x80 | CMD_NUM_CONTROL; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-Module Control-\n");		
		printf("[0x%02X]\n", buff_out[2]);
		printf("Disable Shield (Guard): %i\n", bool(buff_out[2] & 0x20));
		printf("Clear keypad and swipe status: %i\n", bool(buff_out[2] & 0x10));
		printf("Enter Bootloader: %i\n", bool(buff_out[2] & 0x08));
		printf("Enter test mode: %i\n", bool(buff_out[2] & 0x04));
		printf("Disable Key scans: %i\n", bool(buff_out[2] & 0x02));
		printf("Reset Registers (software reset): %i\n", bool(buff_out[2] & 0x01));
	}

	// Keypad One Status
	if (cmd == CMD_NUM_KEYPAD1 || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_KEYPAD1; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-Keypad One Status-\n");
		printf("[0x%02X]\n", buff_out[2]);
		printf("Guard Status  %i\n", bool(buff_out[2] & 0x40));
		printf("Quick Clean Key: %i\n", bool(buff_out[2] & 0x20));
		printf("Stop/Resume Key: %i\n", bool(buff_out[2] & 0x10));
		printf("Speed 4 Key: %i\n", bool(buff_out[2] & 0x08));
		printf("Speed 3 Key: %i\n", bool(buff_out[2] & 0x04));
		printf("Speed 2 Key: %i\n", bool(buff_out[2] & 0x02));
		printf("Speed 1 Key: %i\n", bool(buff_out[2] & 0x01));
	}

	// Keypad Two Status
	if (cmd == CMD_NUM_KEYPAD2 || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_KEYPAD2; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-Keypad Two Status-\n");
		printf("[0x%02X]\n", buff_out[2]);
		printf("Right Arrow Key: %i\n", bool(buff_out[2] & 0x04));
		printf("Left Arrow Key: %i\n", bool(buff_out[2] & 0x02));
		printf("Menu Key : %i\n", bool(buff_out[2] & 0x01));
	}

	// Wheel (Swipe Pad) Last
	if (cmd == CMD_NUM_WHEEL_LAST || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_WHEEL_LAST; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-Wheel (Swipe Pad) Last-\n");
		printf("[0x%02X]\n", buff_out[2]);
	}

	// Wheel (Swipe Pad) Previous
	if (cmd == CMD_NUM_WHEEL_PREV || cmd == CMD_NUM_ALL)
	{		
		buff_in[0] = 0x80 | CMD_NUM_WHEEL_PREV; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-Wheel (Swipe Pad) Previous-\n");
		printf("[0x%02X]\n", buff_out[2]);
	}

	// Guard Key Data
	if (cmd == CMD_NUM_GUARD || cmd == CMD_NUM_ALL)
	{		
		buff_in[0] = 0x80 | CMD_NUM_GUARD; buff_in[1] = 0x80 | CMD_NUM_GUARD_HIGH; buff_in[2] = SPI_DUMMY_BYTE; buff_in[3] = SPI_RD_SIGNATURE;
		SPI_TransiveApp(buff_in, buff_out, 4);
		printf("-Guard Key Data-\n");
		printf("[0x%02X,0x%02X]\n", buff_out[2], buff_out[3]);
		printf("%i\n", (uint16_t)(buff_out[3] & 0x03) << 8 | buff_out[2]);
	}

	//FAIL Free Running Counter (10 ms period)
	// DCM may periodically poll a free running counter register 
	// to verify that the CSIM is still operational
	if (cmd == CMD_NUM_COUNTER || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_COUNTER; buff_in[1] = 0x80 | CMD_NUM_COUNTER_HIGH; buff_in[2] = SPI_DUMMY_BYTE; buff_in[3] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 4);
		printf("-Free Running Counter (10 ms period)-\n");
		printf("[0x%02X,0x%02X]\n", buff_out[2], buff_out[3]);
		printf("%i\n", (uint16_t)buff_out[3] << 8 | buff_out[2]);
	}

	// LED Register (R/W)
	if (cmd == CMD_NUM_LED || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_LED; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-LED Register-\n");
		printf("[0x%02X]\n", buff_out[2]);
		printf("LED Check System: %i\n", bool(buff_out[2] & 0x80));
		printf("LED Timers Active: %i\n", bool(buff_out[2] & 0x40));
		printf("LED Quick Clean Key: %i\n", bool(buff_out[2] & 0x20));
		printf("LED Stop/Resume Key: %i\n", bool(buff_out[2] & 0x10));
		printf("LED Speed 4 Key: %i\n", bool(buff_out[2] & 0x08));
		printf("LED Speed 3 Key: %i\n", bool(buff_out[2] & 0x04));
		printf("LED Speed 2 Key: %i\n", bool(buff_out[2] & 0x02));
		printf("LED Speed 1 Key: %i\n", bool(buff_out[2] & 0x01));
	}

	// LED Blink Register (R/W)
	if (cmd == CMD_NUM_LEDBLINK || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_LEDBLINK; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-LED Blink Register-\n");
		printf("[0x%02X]\n", buff_out[2]);
		printf("LED Check System: %i\n", bool(buff_out[2] & 0x80));
		printf("LED Timers Active: %i\n", bool(buff_out[2] & 0x40));
		printf("LED Quick Clean Key: %i\n", bool(buff_out[2] & 0x20));
		printf("LED Stop/Resume Key: %i\n", bool(buff_out[2] & 0x10));
		printf("LED Speed 4 Key: %i\n", bool(buff_out[2] & 0x08));
		printf("LED Speed 3 Key: %i\n", bool(buff_out[2] & 0x04));
		printf("LED Speed 2 Key: %i\n", bool(buff_out[2] & 0x02));
		printf("LED Speed 1 Key: %i\n", bool(buff_out[2] & 0x01));
	}

	// LED Blink Rate (x10 ms)
	if (cmd == CMD_NUM_LEDBLINKRATE || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_LEDBLINKRATE; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-LED Blink Rate (ms)-\n");
		printf("[0x%02X]\n", buff_out[2]);
		printf("%i\n", buff_out[2] * 10);
	}
	
	// Wheel Sector 0 Output
	if (cmd == CMD_NUM_WHEEL_S0 || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_WHEEL_S0; buff_in[1] = 0x80 | CMD_NUM_WHEEL_S0_HIGH; buff_in[2] = SPI_DUMMY_BYTE; buff_in[3] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 4);
		printf("-Wheel Sector 0 Output-\n");
		printf("[0x%02X,0x%02X]\n", buff_out[2], buff_out[3]);
		printf("%i\n", (uint16_t)(buff_out[3] & 0x03) << 8 | buff_out[2]);
	}

	// Wheel Sector 1 Output
	if (cmd == CMD_NUM_WHEEL_S1 || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_WHEEL_S1; buff_in[1] = 0x80 | CMD_NUM_WHEEL_S1_HIGH; buff_in[2] = SPI_DUMMY_BYTE; buff_in[3] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 4);
		printf("-Wheel Sector 1 Output-\n");
		printf("[0x%02X,0x%02X]\n", buff_out[2], buff_out[3]);
		printf("%i\n", (uint16_t)(buff_out[3] & 0x03) << 8 | buff_out[2]);
	}

	// Wheel Sector 2 Output
	if (cmd == CMD_NUM_WHEEL_S2 || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_WHEEL_S2; buff_in[1] = 0x80 | CMD_NUM_WHEEL_S2_HIGH; buff_in[2] = SPI_DUMMY_BYTE; buff_in[3] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 4);
		printf("-Wheel Sector 2 Output-\n");
		printf("[0x%02X,0x%02X]\n", buff_out[2], buff_out[3]);
		printf("%i\n", (uint16_t)(buff_out[3] & 0x03) << 8 | buff_out[2]);
	}

	// CSIM Signature Byte
	if (cmd == CMD_NUM_SIGNATURE || cmd == CMD_NUM_ALL)
	{
		buff_in[0] = 0x80 | CMD_NUM_SIGNATURE; buff_in[1] = SPI_DUMMY_BYTE; buff_in[2] = SPI_RD_SIGNATURE;		
		SPI_TransiveApp(buff_in, buff_out, 3);
		printf("-CSIM Signature Byte-\n");
		printf("[0x%02X]\n", buff_out[2]);
	}

	return  0;
}

//=========================================================================
// Write Customer protocol registers
//=========================================================================
static uint8_t App_WriteRegisters(int cmd, uint16_t val)
{
	uint8_t buff_in[2];
	uint8_t buff_out[2];
		
	switch (cmd)
	{
		case CMD_NUM_STATUS:
		case CMD_NUM_CONTROL:
		case CMD_NUM_LED:
		case CMD_NUM_LEDBLINK:
		case CMD_NUM_LEDBLINKRATE:
			buff_in[0] = (uint8_t)cmd; buff_in[1] = (uint8_t)val;
			SPI_TransiveApp(buff_in, buff_out, 2);
			break;

		case CMD_NUM_COUNTER:
			buff_in[0] = (uint8_t)cmd; buff_in[1] = (uint8_t)(val >> 8);
			SPI_TransiveApp(buff_in, buff_out, 2);
			buff_in[0] = (uint8_t)cmd + 1; buff_in[1] = (uint8_t)(val & 0xFF);
			SPI_TransiveApp(buff_in, buff_out, 2);
			break;
	}
	return  0;
}



//=========================================================================
// Enter into a bootloader mode
//=========================================================================
static uint8_t App_EnterBootloader(void)
{
	uint8_t buff_in[2] = {0x0B, 0x08};
	uint8_t buff_out[2];
	SPI_TransiveApp(buff_in, buff_out, 2);
	return  0;
}


//=========================================================================
// Commands parser
//=========================================================================
static uint8_t CommandsParser(char* str_in)
{
	int i, ret;
	int len = (int)strlen(str_in);
	int val, val_f = 0;

	// Find additionat value (SET command)
	// Find cmd start pos
	for (i = 0; i < len; i++)
	{
		if (str_in[i] != ' ') break;
	}
	// Find cmd stop pos
	for (; i < len; i++)
	{
		if (str_in[i] == ' ') break;
	}
	// Check max
	if (i < (len - 1)) 
	{
		ret = sscanf(&str_in[i], "%d", &val);
		if ((ret != EOF) && (ret != 0))
		{
			// Additional value found
			val_f = 1;
		}	
	}

	// Command parser
	if (strstr(str_in, CMD_HELP)) 
	{
		printf("%s\n", help_str);
	}
	else
	if (strstr(str_in, CMD_ALL))
	{
		App_ReadRegisters(CMD_NUM_ALL);
	}
	if (strstr(str_in, CMD_UPDATE))
	{
		// Try to update
		SPIMaster_UpdateFW();	
	}
	else
	if (strstr(str_in, CMD_BL_REVISION))
	{
		App_ReadRegisters(CMD_NUM_BL_REVISION);
	}
	else
	if (strstr(str_in, CMD_APP_REVISION))
	{
		App_ReadRegisters(CMD_NUM_APP_REVISION);
	}
	else
	if (strstr(str_in, CMD_STATUS))
	{				
		// Write
		if (val_f) 
		{
			printf("Write new value\n");
			App_WriteRegisters(CMD_NUM_STATUS, val);
		}	
		App_ReadRegisters(CMD_NUM_STATUS);
	}
	else
	if (strstr(str_in, CMD_CONTROL))
	{				
		// Write
		if (val_f) 
		{
			printf("Write new value\n");
			App_WriteRegisters(CMD_NUM_CONTROL, val);
		}	
		App_ReadRegisters(CMD_NUM_CONTROL);
	}
	else
	if (strstr(str_in, CMD_KEYPAD1))
	{
		App_ReadRegisters(CMD_NUM_KEYPAD1);
	}
	else
	if (strstr(str_in, CMD_KEYPAD2))
	{
		App_ReadRegisters(CMD_NUM_KEYPAD2);
	}
	else
	if (strstr(str_in, CMD_WHEEL_LAST))
	{
		App_ReadRegisters(CMD_NUM_WHEEL_LAST);
	}
	else
	if (strstr(str_in, CMD_WHEEL_PREV))
	{
		App_ReadRegisters(CMD_NUM_WHEEL_PREV);
	}
	else
	if (strstr(str_in, CMD_GUARD))
	{
		App_ReadRegisters(CMD_NUM_GUARD);
	}
	else
	if (strstr(str_in, CMD_COUNTER))
	{				
		// Write
		if (val_f) 
		{
			printf("Write new value\n");
			App_WriteRegisters(CMD_NUM_COUNTER, val);
		}	
		App_ReadRegisters(CMD_NUM_COUNTER);
	}
	else
	if (strstr(str_in, CMD_LEDBLINK))
	{				
		// Write
		if (val_f) 
		{
			printf("Write new value\n");
			App_WriteRegisters(CMD_NUM_LEDBLINK, val);
		}	
		App_ReadRegisters(CMD_NUM_LEDBLINK);
	}
	else
	if (strstr(str_in, CMD_LED))
	{				
		// Write
		if (val_f) 
		{
			printf("Write new value\n");
			App_WriteRegisters(CMD_NUM_LED, val);
		}	
		App_ReadRegisters(CMD_NUM_LED);
	}
	else
	if (strstr(str_in, CMD_LEDBLINKRATE))
	{				
		// Write
		if (val_f) 
		{
			printf("Write new value\n");
			App_WriteRegisters(CMD_NUM_LEDBLINKRATE, val);
		}	
		App_ReadRegisters(CMD_NUM_LEDBLINKRATE);
	}
	else
	if (strstr(str_in, CMD_WHEEL_S0))
	{
		App_ReadRegisters(CMD_NUM_WHEEL_S0);
	}
	else
	if (strstr(str_in, CMD_WHEEL_S1))
	{
		App_ReadRegisters(CMD_NUM_WHEEL_S1);
	}
	else
	if (strstr(str_in, CMD_WHEEL_S2))
	{
		App_ReadRegisters(CMD_NUM_WHEEL_S2);
	}
	else
	if (strstr(str_in, CMD_SIGNATURE))
	{
		App_ReadRegisters(CMD_NUM_SIGNATURE);
	}


	return 0;
}

//=========================================================================
// Main menu
//=========================================================================
int SPIMaster_Menu(void)
{
	char str_scan[50];

	while (1)
	{
		printf("\n>");
		fgets(str_scan, 49, stdin);
		CommandsParser(str_scan);

		//App_Tests();
		//Sleep(1000);
	}

	return 0;
}
//=========================================================================
// Start Software Update sample
//=========================================================================
int SPIMaster_UpdateFW(void)
{
	uint32_t cnt = 0;
	uint16_t timeout, page = 0;
	char filename[50] = "mainapp.bin";
	uint32_t i, num, ans;
	char str_scan[50];


	// Read firmware name
	printf("Enter firmware name: ");
	scanf("%s", filename);
	
	// Start to update
	// Try to open
	FILE *file = fopen(filename, "rb");
	if (!file)
	{
		printf("\tNo File '%s' found!\n", filename);
		return 1;
	}

	// Get file size
	fseek(file, 0, SEEK_END); // seek to end of file
	uint32_t fmw_length = ftell(file); // get current file pointer
	fseek(file, 0, SEEK_SET); // seek back to beginning of file	

	// Move file into a buffer
	uint8_t *fmw = new uint8_t[fmw_length];
	if (fmw == NULL) 
	{
		printf("\tNo memory!\n");
		return 2;
	}

	uint8_t *fmw_p = fmw;
	uint32_t tmp, len = 0;
	while (tmp = (uint32_t)fread(fmw_p, 1, fmw_length - len, file))
	{
		fmw_p += tmp;
		len += tmp;
	}
	fclose(file);

	// Flash a new Firmware
	SPIMaster_ReadStatusReg();
	Sleep(10);
	SPIMaster_ReadStatusReg();
	Sleep(10);
	SPIMaster_ReadStatusReg();
	Sleep(10);

	// If no bootloader 
	// initiate APP boot into bootloader mode
	if (!SPIMaster_IsBootloader())
	{	
		// Print App register info
		App_ReadRegisters(CMD_NUM_ALL);
		//return 0;
		// Clear update rdy flag & Reset MCU 
		App_EnterBootloader();

		Sleep(2000);
	}

	// Print bootloder version
	ans = SPIMaster_GetVersion();
	printf("Bootloader version: 0x%02X\n", ans);

	printf("Write Data\n");
	i = 0; 
	while (len)
	{
		timeout = 0;
		while (SPIMaster_ReadStatusReg() != 0xB1)
		{
			Sleep(1);
			if (timeout++ > 100) 
			{
				printf("Rx Timeout!\n");
				// Delete buffer
				delete[] fmw;
				return 3; //Timeout
			}
		}

		if (len > FLASH_PAGE_LENGTH)
		{
			num = FLASH_PAGE_LENGTH;
		}
		else
		{
			num = len;
		}		
		SPIMaster_WriteData(FLASH_STARTADDR + APP_STARTADDR + i, &fmw[i], num);
	
		// Output progress bar
		SPIMaster_PogressBar(i, fmw_length);

		i += num;
		len -= num;
	}
	SPIMaster_PogressBar(fmw_length, fmw_length);
	
	printf("\nRead/Compare Data\n");
	i = 0; len = fmw_length;
	while (len)
	{
		timeout = 0;
		while (SPIMaster_ReadStatusReg() != 0xB1)
		{
			Sleep(1);
			if (timeout++ > 100)
			{
				printf("Rx Timeout!\n");
				// Delete buffer
				delete[] fmw;
				return 3; //Timeout
			}
		}

		if (len > FLASH_PAGE_LENGTH)
		{
			num = FLASH_PAGE_LENGTH;
		}
		else
		{
			num = len;
		}
		SPIMaster_ReadData(FLASH_STARTADDR + APP_STARTADDR + i, io_buf, num);
		// Compare
		if (memcmp(io_buf, &fmw[i], num) != 0)
		{
			printf("\nVerification failed!\n");

			// Delete buffer
			delete[] fmw;
			return 3;
		}

		// Output progress bar
		SPIMaster_PogressBar(i, fmw_length);

		i += num;
		len -= num;
	}

	SPIMaster_PogressBar(fmw_length, fmw_length);
	printf("\nFlash OK!\n");	

	SPIMaster_SetUpdateReadyFlag();
	SPIMaster_ResetMCU();

	// Delete buffer
	delete[] fmw;

	//Firmware done OK
	return 0; 
}