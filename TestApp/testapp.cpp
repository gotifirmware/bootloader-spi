#include "stdafx.h"
#include "windows.h"

#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#include "aardvark.h"
#include "SPIMaster.h" 


//=========================================================================
// CONSTANTS
//=========================================================================

//=========================================================================
// VARIABLES
//=========================================================================

//=========================================================================
// FUNCTIONS
//=========================================================================
static void ReadCallback(const uint8_t* data)
{

}

//=========================================================================
// MAIN
//=========================================================================
int main(int argc, char *argv[]) 
{
	// Init
	if (SPIMaster_Init(ReadCallback) != PROT_COMM_Success)
	{
		return 1;
	}

	// Start main menu
	SPIMaster_Menu();

	// Try to update
	//SPIMaster_UpdateFW();	

    // Close the device
	SPIMaster_Close();

	system("PAUSE");
    return 0;
}

