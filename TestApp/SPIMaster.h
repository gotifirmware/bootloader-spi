#ifndef __SPI_MASTER_MODULE_H__
#define __SPI_MASTER_MODULE_H__

#ifdef __cplusplus
extern "C" {
#endif
	 
#include <string.h>
#include <stdio.h>
#include <wchar.h>
#include <stdint.h>
#include <windows.h>

// Buff 
#define IO_BUFFER_SIZE_MAX				128

#define  CMD_HELP				"help"				// print command list
#define  CMD_ALL				"all"				// Show all posssible information
#define  CMD_UPDATE				"update"			// Update firmware
//---
#define  CMD_BL_REVISION		"blversion"			// Bootloader Revision(0 if no bootloader)
#define  CMD_APP_REVISION		"version"			// Application Revision
#define  CMD_STATUS				"status"			// Module Status
#define  CMD_CONTROL			"control"			// Module Control
#define  CMD_KEYPAD1			"key1"				// Keypad One Status
#define  CMD_KEYPAD2			"key2"				// Keypad Two Status
#define  CMD_WHEEL_LAST			"wheellast"			// Wheel (Swipe Pad) Last(i.e., most recent) Position
#define  CMD_WHEEL_PREV			"wheelprev"			// Wheel (Swipe Pad) Previous Position
#define  CMD_GUARD				"guard"				// 5.1.9.	Guard Key Data; Address: 16 � 17 (0x10 � 0x11)
#define  CMD_COUNTER			"counter"			// Free Running Counter(10 ms period)
#define  CMD_LED				"led"				// LED Register
#define  CMD_LEDBLINK			"ledblink"			// LED Blink Register
#define  CMD_LEDBLINKRATE		"blinkrate"			// LED Blink Rate(10 ms units)
#define  CMD_WHEEL_S0			"wheel0"			// Wheel Sector 0 Output
#define  CMD_WHEEL_S1			"wheel1"			// Wheel Sector 1 Output
#define  CMD_WHEEL_S2			"wheel2"			// Wheel Sector 2 Output
#define  CMD_SIGNATURE			"signature"			// Signature byte for the CSIM; always reads 0x47
//#define  CMD_ Packet Protocol Read / Write Header
	

typedef enum
{
	CMD_NUM_BL_REVISION = 0,
	CMD_NUM_APP_REVISION = 5,
	CMD_NUM_STATUS = 10,
	CMD_NUM_CONTROL,
	CMD_NUM_KEYPAD1,
	CMD_NUM_KEYPAD2,
	CMD_NUM_WHEEL_LAST,
	CMD_NUM_WHEEL_PREV,
	CMD_NUM_GUARD,
	CMD_NUM_GUARD_HIGH,
	CMD_NUM_COUNTER,
	CMD_NUM_COUNTER_HIGH,
	CMD_NUM_LED,
	CMD_NUM_LEDBLINK,
	CMD_NUM_LEDBLINKRATE,
	CMD_NUM_WHEEL_S0,
	CMD_NUM_WHEEL_S0_HIGH,
	CMD_NUM_WHEEL_S1,
	CMD_NUM_WHEEL_S1_HIGH,
	CMD_NUM_WHEEL_S2,
	CMD_NUM_WHEEL_S2_HIGH,
	CMD_NUM_SIGNATURE = 30,
	//---
	CMD_NUM_HELP = 64,
	CMD_NUM_ALL,
	CMD_NUM_UPDATE
} CMD_NUNB_Type;

// Signatures
#define		SPI_DUMMY_BYTE				0x1E
#define		SPI_WR_SIGNATURE			0x47 // 'G'
#define		SPI_RD_SIGNATURE			0x48 // 'H'


// Commands
#define  BL_CMD_NONE                    0
#define  BL_CMD_WRITE                   1
#define  BL_CMD_READ                    2
#define  BL_CMD_STATUSR                 3
#define  BL_CMD_RESET                   4
#define  BL_CMD_SET_UPDRDY_FLAG         5
#define  BL_CMD_CLR_UPDRDY_FLAG         6
#define  BL_CMD_GET_VERSION             7
#define  BL_CMD_ISBOOTLOADER            8

// Blink LED (no interrupt) FlashStrartAddr = 0x0A00
#define FLASH_STARTADDR					0x0000
//#define FLASH_STARTADDR				0x8000
#define FLASH_PAGE_LENGTH				64
#define APP_STARTADDR					0x1400


typedef enum
{
	PROT_COMM_Success = 0,
	PROT_COMM_Error = -1
} PROT_COMM_RetType;

// Function prototype 
int SPIMaster_Init(void(*f_ptr)(const uint8_t* SPIMaster_ReadData));
int SPIMaster_Menu(void);
int SPIMaster_UpdateFW(void);
void SPIMaster_Close(void);


#ifdef __cplusplus
}
#endif
#endif

