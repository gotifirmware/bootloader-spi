
//////////////////////////////////////////////////////////////
//
//	Name:
//			haySquare.c
//
//////////////////////////////////////////////////////////////

// --- INCLUDES ---
#include "haySquare.h"
#include "hayward.h"


// --- DEFINES ---
#define	 DELAY_I2C			for( int8_t counter = 0; counter < 19; counter++);
#define  DATA_REGISTER_ADDRESS		0x02

// --- new LCD controller ---
//#define		DELAY_TWO_I2C		for( int8_t counter = 0; counter < 20; counter++);

// --- GLOBALS ---
int8_t		modAddr[] =  { 0, 1, 0, 0, 0, 1, 0, 0 };
uint8_t		blinkStateOFF;

//extern		blinkTimerSetValue;

volatile bool						blinkingENABLE;
uint8_t								blinkStateON;
bool								trigger;
uint8_t	                            configRegisterAddress;
uint8_t								configPort_0, configPort_1;


// -----------------  B O D Y  -------------------------------

//////////////////////////////////////////////////////////////
//
//	Name:
//		I2C sendModuleAddress
//
//////////////////////////////////////////////////////////////
void sendModuleAddress(void)
{
	int counter;

	CLK_HIGH		// --- initial state ---
	SDA_HIGH
	DELAY_I2C

	SDA_LOW			// --- START condition ---
	DELAY_I2C
	CLK_LOW
	DELAY_I2C


	for(counter = 0; counter < 8; counter++)
	{
		if( modAddr[counter] == 0 ) SDA_LOW
		else SDA_HIGH
		DELAY_I2C
		CLK_HIGH
		DELAY_I2C
		DELAY_I2C
		CLK_LOW
		DELAY_I2C
	}

	SDA_LOW		// --- wait for the ACK ---
	DELAY_I2C
	CLK_HIGH
	DELAY_I2C
	DELAY_I2C
	CLK_LOW
	DELAY_I2C

} // --- eof sendModuleAddress9 ) ---


//////////////////////////////////////////////////////////////
//
//	Name:
//		I2C sendByte
//
//	Purpose:
//		Sends Command Bytes and Data Bytes
//
//////////////////////////////////////////////////////////////
void sendByte(uint8_t byteValue)
{
	int8_t counter;

	for(counter = 0; counter < 8; counter++)
	{
		if( (byteValue & 0x80) == 0x80 ) SDA_HIGH
		else  SDA_LOW

		DELAY_I2C
		CLK_HIGH
		DELAY_I2C
		DELAY_I2C
		CLK_LOW
		DELAY_I2C

		byteValue = byteValue<<1;
	}

	SDA_LOW		// --- wait for the ACK ---
	DELAY_I2C
	CLK_HIGH
	DELAY_I2C
	DELAY_I2C
	CLK_LOW
	DELAY_I2C


} // --- eof sendByte() ---




//////////////////////////////////////////////////////////////
//
//	Name:
//		I2C sendStopCondition
//
//////////////////////////////////////////////////////////////
void sendStopCondition(void)
{
	DELAY_I2C
	CLK_HIGH
	DELAY_I2C
	SDA_HIGH
	DELAY_I2C
	CLK_LOW
	DELAY_I2C
}



//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
void driveLEDs(uint8_t byteValue)
{
	// --- local data ---
	uint8_t 	i2cByte;

	// --- body ---
	i2cByte = byteValue;
	sendModuleAddress();
	sendByte(DATA_REGISTER_ADDRESS);  // --- 0x02: the Output Reg. 0 address above ---
	sendByte(255 - i2cByte);		  // --- this will shoot to the Port 0  Output Registerr ---  KEEP THIS FOR ALFA: Bill inverted the outputs --
	//sendByte(i2cByte);		      // --- this will shoot to the Upper LEDs buffer ---
	sendByte(0x00);                   // --- the Output Register at 0x03 is not used now  ---
	sendStopCondition();
}


/////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////
void makeBlink(void)
{

	if(commandsRegistry[21] != 0) blinkingENABLE = true;
	else blinkingENABLE = false;

	if( blinkingENABLE == true )
	{
		blinkStateON = commandsRegistry[20] & (~commandsRegistry[21]);

		if(trigger == true) driveLEDs( blinkStateON );
		else driveLEDs( commandsRegistry[20] );

		trigger = !trigger;
	}


}// --- eof makeBlink() ---


void i2cConfig(void)
{
// --- register setup ---
	configRegisterAddress = 0x06;   // --- address of Config. Reg. for Port 0 ---
	configPort_0		  = 0x00;   // --- all outputs ---
	configPort_1		  = 0x00;	// --- all outputs ( currently not used ) ---

// --- BODY ---
	sendModuleAddress();
	sendByte(configRegisterAddress); // --- address 0x06 ---
	sendByte(configPort_0);          // --- reg_at_06 loaded with 0's ---
	sendByte(configPort_1);			 // --- reg_at_07 loaded with 0's ( optional )
	sendStopCondition();
}


/*
void sendByteNew(uint8_t byteValue)
{
	int8_t counter;

	SHIFT_CLOCK_LOW
	SHIFT_DATA_LOW
	LOAD_CLOCK_LOW
	DELAY_TWO_I2C

	for(counter = 0; counter < 8; counter++)
	{
		// --- pick up bit_7 only ---
		if( (byteValue & 0x80) == 0x80 ) SHIFT_DATA_HIGH
		else  SHIFT_DATA_LOW

		DELAY_TWO_I2C
		SHIFT_CLOCK_HIGH
		DELAY_TWO_I2C
		DELAY_TWO_I2C
		SHIFT_CLOCK_LOW
		DELAY_TWO_I2C

		byteValue = byteValue<<1;
	}
	// --- build the Transfer_Clock pulse ---
	LOAD_CLOCK_HIGH
	DELAY_TWO_I2C
	DELAY_TWO_I2C
	LOAD_CLOCK_LOW
	DELAY_TWO_I2C

} // --- eof sendByte() ---
*/
// --- EOF ---




