#ifndef F_CPU
#define F_CPU 20000000
#endif

// Uncomment for SLOW
// Comment for QUICK
//#define SLOW_OPERATION

// --- INCLUDES ---
#include "haySquare.h"
#include "bl_protocol_slave.h"

static uint8_t tmp = 0x01;

#pragma vector=RTC_CNT_vect
 __interrupt void RTC_CNT_vect_interrupt(void)
{
        // Overflow interrupt flag has to be cleared manually
        RTC.INTFLAGS = RTC_OVF_bm;

        // LED output
        driveLEDs(tmp);
        tmp <<= 1;
        if (tmp >= 0x08) tmp = 0x01;
}


void _delay_sec(void)
{
    uint32_t tt=0;
#ifdef SLOW_OPERATION
    while(++tt < 100000)
#else
    while(++tt < 1000000)
#endif
    {
      __no_operation();
    }
}

int main(void)
{
        // Interrupt Vector Select: disable (INTVECT in APP section!!!)
//        ccp_write_io((void *)&(CPUINT.CTRLA), 0x00);

    // --- configure the LED Controller here -------------------------
	// --- CLK ---
	PORTB_set_pin_pull_mode(3, PORT_PULL_OFF);
	PORTB_set_pin_dir(3, PORT_DIR_OUT);
	PORTB_set_pin_level(3, true);
	// --- SDA ---
	PORTB_set_pin_pull_mode(2, PORT_PULL_OFF);
	PORTB_set_pin_dir(2, PORT_DIR_OUT);
	PORTB_set_pin_level(2, true);

        i2cConfig();

        // RTC
        RTC.CTRLA = RTC_PRESCALER_DIV32_gc  // 32
                    | 1 << RTC_RTCEN_bp     // Enable: enabled
                    | 0 << RTC_RUNSTDBY_bp; // Run In Standby: disabled
#ifdef SLOW_OPERATION
        RTC.PER = 500; //Period
#else
        RTC.PER = 100;
#endif

        RTC.INTCTRL = 0 << RTC_CMP_bp    // Compare Match Interrupt enable: disabled
                      | 1 << RTC_OVF_bp; // Overflow Interrupt enable: enabled

        while (RTC.STATUS > 0) { // Wait for all register to be synchronized
        }

        system_init();
        bSlave_Init();

        __enable_interrupt();

	while (1) {
/*
	driveLEDs(0x01);
	_delay_sec();
	driveLEDs(0x02);
	_delay_sec();
	driveLEDs(0x04);
	_delay_sec();
	driveLEDs(0x08);
	_delay_sec();
*/
	continue;

	}
}
