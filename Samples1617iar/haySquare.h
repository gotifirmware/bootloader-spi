

#ifndef HAYWARD_I2C_H
#define HAYWARD_I2C_H

////////////////////////////////////////////////////////////////
//
//	Name:
//			haySquare.h
//
////////////////////////////////////////////////////////////////
#include "atmel_start.h"

// --- CLK ---
#define  CLK_LOW			PORTB_set_pin_level(3, false);
#define  CLK_HIGH			PORTB_set_pin_level(3, true);

// --- SDA ---
#define  SDA_LOW			PORTB_set_pin_level(2, false);
#define  SDA_HIGH			PORTB_set_pin_level(2, true);

/*
// --- the new I2C controller ---
#define		SHIFT_CLOCK_LOW		PORTB_set_pin_level(3, false);
#define		SHIFT_CLOCK_HIGH	PORTB_set_pin_level(3, true);

#define		SHIFT_DATA_LOW		PORTB_set_pin_level(2, false);
#define		SHIFT_DATA_HIGH		PORTB_set_pin_level(2, true);

#define		LOAD_CLOCK_LOW		PORTB_set_pin_level(6, false);
#define		LOAD_CLOCK_HIGH		PORTB_set_pin_level(6, true);
// --- end of new controller ---
*/

// --- I2C: FUNCTION PROTOTYPES ---
void i2cConfig(void);
void sendModuleAddress(void);
void sendByte(uint8_t byteValue);
void sendStopCondition(void);
//
void sendByteNew(uint8_t byteValue);

void driveLEDs(uint8_t byteValue);
void makeBlink(void);

#endif // --- HAYWARD_I2C_H

// --- EOF ---





