/**
 * \file
 *
 * \brief NVMCTRL Basic driver implementation.
 *
 *
 * Copyright (C) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 *
 */

#include <nvmctrl_basic.h>

typedef struct FLASH_0_descriptor_s {
	nvmctrl_status_t       status;
	uint8_t *              eeprom;
	uint8_t *              data;
	uint8_t                size;
	eeprom_write_done_cb_t cb;
} FLASH_0_descriptor_t;

static volatile FLASH_0_descriptor_t FLASH_0_desc;

ISR(NVMCTRL_EE_vect)
{

	/* The interrupt flag has to be cleared manually */
	NVMCTRL.INTFLAGS = NVMCTRL_EEREADY_bm;

	if (FLASH_0_desc.size == 0) {
		FLASH_0_desc.status = NVM_OK;
		NVMCTRL.INTCTRL &= ~NVMCTRL_EEREADY_bm;
		if (FLASH_0_desc.cb != NULL) {
			FLASH_0_desc.cb();
		}
	} else {
		while (1) {
			/* Write byte to page buffer */
			*FLASH_0_desc.eeprom++ = *FLASH_0_desc.data++;
			FLASH_0_desc.size--;
			// If we have filled an entire page or written last byte to a partially filled page
			if ((((uintptr_t)FLASH_0_desc.eeprom % EEPROM_PAGE_SIZE) == 0) || (FLASH_0_desc.size == 0)) {
				/* Erase written part of page and program with desired value(s) */
				ccp_write_spm((void *)&NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);
				break;
			}
		}
	}
}

/**
 * \brief Register a callback function to be called by EEPROM ISR after block write has finished.
 *
 * \param[in] f Pointer to function to be called
 *
 * \return Nothing.
 */
void FLASH_0_register_callback(eeprom_write_done_cb_t f)
{
	FLASH_0_desc.cb = f;
}

int8_t FLASH_0_init()
{

	// NVMCTRL.CTRLB = 0 << NVMCTRL_APCWP_bp /* Application code write protect: disabled */
	//		 | 0 << NVMCTRL_BOOTLOCK_bp; /* Boot Lock: disabled */

	// NVMCTRL.INTCTRL = 0 << NVMCTRL_EEREADY_bp; /* EEPROM Ready: disabled */

	FLASH_0_desc.cb = NULL;
	return 0;
}

uint8_t FLASH_0_read_eeprom_byte(eeprom_adr_t eeprom_adr)
{
	// Wait until any write operation has finished
	while (FLASH_0_desc.status == NVM_BUSY)
		;

	return *(uint8_t *)(EEPROM_START + eeprom_adr);
}

nvmctrl_status_t FLASH_0_write_eeprom_byte(eeprom_adr_t eeprom_adr, uint8_t data)
{
	return (FLASH_0_write_eeprom_block(eeprom_adr, &data, 1));
}

void FLASH_0_read_eeprom_block(eeprom_adr_t eeprom_adr, uint8_t *data, size_t size)
{
	// Wait until any write operation has finished
	while (FLASH_0_desc.status == NVM_BUSY)
		;

	memcpy(data, (uint8_t *)(EEPROM_START + eeprom_adr), size);
}

nvmctrl_status_t FLASH_0_write_eeprom_block(eeprom_adr_t eeprom_adr, uint8_t *data, size_t size)
{

	// Wait until any write operation has finished
	while (FLASH_0_desc.status == NVM_BUSY)
		;

	FLASH_0_desc.eeprom = (uint8_t *)(EEPROM_START + eeprom_adr);
	FLASH_0_desc.data   = data;
	FLASH_0_desc.size   = size;
	FLASH_0_desc.status = NVM_BUSY;

	/* Clear page buffer */
	ccp_write_spm((void *)&NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEBUFCLR_gc);
	NVMCTRL.INTCTRL |= NVMCTRL_EEREADY_bm;

	return NVM_OK;
}

bool FLASH_0_is_eeprom_ready()
{
	return (FLASH_0_desc.status == NVM_BUSY);
}

uint8_t FLASH_0_read_flash_byte(flash_adr_t flash_adr)
{
	uint8_t tmp = *(uint8_t *)(MAPPED_PROGMEM_START + flash_adr);
	//return *(uint8_t *)(MAPPED_PROGMEM_START + flash_adr);
	return tmp;
}

nvmctrl_status_t FLASH_0_write_flash_byte(flash_adr_t flash_adr, uint8_t *ram_buffer, uint8_t data)
{

	// Create a pointer to the start of the flash page containing the byte to write
	volatile uint8_t *start_of_page = (uint8_t *)((MAPPED_PROGMEM_START + flash_adr) & ~(PROGMEM_PAGE_SIZE - 1));
	uint8_t           i;

	// Read contents of Flash page containing the target address to the page buffer
	for (i               = 0; i < PROGMEM_PAGE_SIZE; i++)
		start_of_page[i] = start_of_page[i];

	// Write the new byte value to the correct address in the page buffer
	*(uint8_t *)(MAPPED_PROGMEM_START + flash_adr) = data;

	// Erase and write the flash page
	ccp_write_spm((void *)&NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);

	if (NVMCTRL.STATUS & NVMCTRL_WRERROR_bm)
		return NVM_ERROR;
	else
		return NVM_OK;
}

nvmctrl_status_t FLASH_0_erase_flash_page(flash_adr_t flash_adr)
{

	// Create a pointer in unified memory map to the page to erase
	uint8_t *data_space = (uint8_t *)(MAPPED_PROGMEM_START + flash_adr);

	// Perform a dummy write to this address to update the address register in NVMCTL
	*data_space = 0;

	// Erase the flash page
	ccp_write_spm((void *)&NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASE_gc);

	if (NVMCTRL.STATUS & NVMCTRL_WRERROR_bm)
		return NVM_ERROR;
	else
		return NVM_OK;
}

nvmctrl_status_t FLASH_0_write_flash_page(flash_adr_t flash_adr, uint8_t *data)
{

	// Create a pointer in unified memory map to the page to write
	uint8_t *data_space = (uint8_t *)(MAPPED_PROGMEM_START + flash_adr);

	// Write data to the page buffer
	memcpy(data_space, data, PROGMEM_PAGE_SIZE);

	// Write the flash page
	ccp_write_spm((void *)&NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEWRITE_gc);

	if (NVMCTRL.STATUS & NVMCTRL_WRERROR_bm)
		return NVM_ERROR;
	else
		return NVM_OK;
}

nvmctrl_status_t FLASH_0_write_flash_block(flash_adr_t flash_adr, uint8_t *data, size_t size, uint8_t *ram_buffer)
{

	// Create a pointer in unified memory map to the start of first page to modify
	volatile uint8_t *data_space   = (uint8_t *)((MAPPED_PROGMEM_START + flash_adr) & ~(PROGMEM_PAGE_SIZE - 1));
	uint8_t           start_offset = flash_adr % PROGMEM_PAGE_SIZE;
	uint8_t           i;

	// Step 1:
	// Fill page buffer with contents of first flash page to be written up
	// to the first flash address to be replaced by the new contents
	for (i = 0; i < start_offset; i++) {
		*data_space = *data_space;
		data_space++;
	}

	// Step 2:
	// Write all of the new flash contents to the page buffer, writing the
	// page buffer to flash every time the buffer contains a complete flash
	// page.
	while (size > 0) {
		*data_space++ = *data++;
		size--;
		if (((uintptr_t)data_space % PROGMEM_PAGE_SIZE) == 0) {
			// Erase and write the flash page
			ccp_write_spm((void *)&NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);
			if (NVMCTRL.STATUS & NVMCTRL_WRERROR_bm)
				return NVM_ERROR;
		}
	}

	// Step 3:
	// After step 2, the page buffer may be partially full with the last
	// part of the new data to write to flash. The remainder of the flash page
	// shall be unaltered. Fill up the remainder
	// of the page buffer with the original contents of the flash page, and do a
	// final flash page write.
	while (1) {
		*data_space = *data_space;
		data_space++;
		if (((uintptr_t)data_space % PROGMEM_PAGE_SIZE) == 0) {
			// Erase and write the last flash page
			ccp_write_spm((void *)&NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);
			if (NVMCTRL.STATUS & NVMCTRL_WRERROR_bm)
				return NVM_ERROR;
			else
				return NVM_OK;
		}
	}
}

nvmctrl_status_t FLASH_0_write_flash_stream(flash_adr_t flash_adr, uint8_t data, bool finalize)
{

	bool final_adr_in_page = ((flash_adr & (PROGMEM_PAGE_SIZE - 1)) == (PROGMEM_PAGE_SIZE - 1));

	// Write the new byte value to the correct address in the page buffer
	*(uint8_t *)(MAPPED_PROGMEM_START + flash_adr) = data;

	if (final_adr_in_page || finalize)
		// Erase and write the flash page
		ccp_write_spm((void *)&NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);

	if (NVMCTRL.STATUS & NVMCTRL_WRERROR_bm)
		return NVM_ERROR;
	else
		return NVM_OK;
}
