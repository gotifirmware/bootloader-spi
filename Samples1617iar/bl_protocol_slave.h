#ifndef __BL_PROTOCOL_SLAVE_H
#define __BL_PROTOCOL_SLAVE_H

//#include	<nvmctrl_basic.h>
#include    <port.h>
#include    <stdint.h>
#include    <stdio.h>
#include    "spi_basic.h"
#include    "rstctrl.h"

// Define Fuses BOOTEND value here
// The section should be large enough to hold Bootloader program (bytes)
#define BL_BOOT_MIN_SIZE				(256*20)
// The section should be large enough to hold Application program (bytes)
#define BL_APP_MIN_SIZE					(256*10)


// Commands
#define  BL_CMD_NONE                    0
#define  BL_CMD_WRITE                   1
#define  BL_CMD_READ                    2
#define  BL_CMD_STATUSR                 3
#define  BL_CMD_RESET                   4
#define  BL_CMD_SET_UPDRDY_FLAG         5
#define  BL_CMD_CLR_UPDRDY_FLAG         6
#define  BL_CMD_GET_VERSION             7
#define  BL_CMD_ISBOOTLOADER            8

// Default Bootloader ID
#define  BL_ID_MASK                     0xB0
// Number of address bytes (MSB first)
#define  BL_ADDR_BYTES_N                4

// Status register flags
#define  BL_SR_ID3                      7
#define  BL_SR_ID2                      6
#define  BL_SR_ID1                      5
#define  BL_SR_ID0                      4
//Reserved bits
#define  BL_SR_RES2                     3
#define  BL_SR_RES1                     2
#define  BL_SR_RES0                     1
// Ready flag
#define  BL_SR_RDY                      0


// Define EEPROM address and 16b 'magic number' to start App (without bootloader)
#define  BL_APP_BOOTFLAG_ADDR           0x0010
#define  BL_APP_BOOTFLAG_MAGICWORD		0x5A


// Public functions
void bSlave_Init(void);
void bSlave_Main(void);


#endif // __BL_PROTOCOL_SLAVE_H

