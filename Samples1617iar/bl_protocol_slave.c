#include <atomic.h>
//#include <avr/pgmspace.h>
#include "bl_protocol_slave.h"
#include "nvmctrl_basic.h"
#include "clock_config.h"


// Locals
uint32_t wr_addr;
uint32_t rd_addr;
uint8_t cmd;
uint8_t input_buff[6];
uint8_t addr_buff = 0;
volatile uint8_t input_cnt = 0;
volatile uint8_t ready_f = 1;

uint8_t ram_buff[PROGMEM_PAGE_SIZE];


//--------------------------------------------------
// Chip Select (/CS) release event interrupt
//--------------------------------------------------
//ISR( PORTA_PORT_vect )
#pragma vector=PORTA_PORT_vect
 __interrupt void PORTA_PORT_vect_interrupt(void)
{
	// Clr interrupt flag
	PORTA.INTFLAGS = PORTA.INTFLAGS;

	// If Write command ends
	if ((cmd == BL_CMD_WRITE) && (addr_buff))
	{
		// Mark 'Busy'
		ready_f = 0;
	}

	input_cnt = 0;
	addr_buff = 0;

}

//--------------------------------------------------
// Bootloader Commands Parser
//--------------------------------------------------
void bSlave_ISR(uint8_t data_in)
{
    // Add new start data (cmd + addr)
    if (input_cnt <= 5)
    {
        input_buff[input_cnt++] = data_in;
    }
    // Get command
    cmd = input_buff[0];

    switch (cmd)
    {
        case BL_CMD_STATUSR:
            if (input_cnt == 1)
            {
                SPI_0_write(BL_ID_MASK | (ready_f << BL_SR_RDY));
            }
            break;

        case BL_CMD_RESET:
            if (input_cnt == 2)
            {
                if (data_in == BL_CMD_RESET)
                {
					//__disable_interrupt();
                    // Start to Reset MCU Now!
					RSTCTRL_reset();
					//RSTCTRL_call_application();

					// Never return, this saves code space since stack
					// need not be preserved on function entry.
					while (1);
                }
            }
            break;

        case BL_CMD_SET_UPDRDY_FLAG:
			if (input_cnt == 2)
			{
				if (data_in == BL_CMD_SET_UPDRDY_FLAG)
				{
					// Setup Update ready flag
					FLASH_0_write_eeprom_byte(BL_APP_BOOTFLAG_ADDR + 0, BL_APP_BOOTFLAG_MAGICWORD);
				}
			}
			break;

        case BL_CMD_CLR_UPDRDY_FLAG:
			if (input_cnt == 2)
			{
				if (data_in == BL_CMD_CLR_UPDRDY_FLAG)
				{
					// Setup Update ready flag
					FLASH_0_write_eeprom_byte(BL_APP_BOOTFLAG_ADDR + 0, 0xFF);
				}
			}
        break;


        case BL_CMD_GET_VERSION:
            if (input_cnt == 1)
            {
                SPI_0_write(0x12);
            }
            break;

        case BL_CMD_ISBOOTLOADER:
            if (input_cnt == 1)
            {
                SPI_0_write(0x00); //No bootloader (app)
            }
            break;


        default:
            input_cnt = 0;
			addr_buff = 0;
    }

}

//--------------------------------------------------
// Bootloader Init function
//--------------------------------------------------
void bSlave_Init(void)
{
	SPI_0_init(bSlave_ISR);
}

//--------------------------------------------------
// Main Bootloader routine function
//--------------------------------------------------
void bSlave_Main(void)
{
}
