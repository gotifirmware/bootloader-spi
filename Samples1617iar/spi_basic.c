#include <spi_basic.h>
#include <atmel_start_pins.h>

#include <stdio.h>

typedef struct SPI_0_descriptor_s {
	spi_transfer_status_t status;
} SPI_0_descriptor_t;

static SPI_0_descriptor_t SPI_0_desc;

// Receive  function callback
static SPI_receive_byte_callback_t CallbackFunc;


//ISR( SPI0_INT_vect )
#pragma vector=SPI0_INT_vect
 __interrupt void SPI0_INT_vect_interrupt(void)
{
	// Call external function
	//if (SPI0.INTFLAGS & SPI_RXCIF_bm)
	if (SPI0.INTFLAGS)	SPI0.INTFLAGS = 0x00;
	//if (SPI0.INTFLAGS & SPI_IE_bm)
	{
		CallbackFunc(SPI0.DATA);
	}

//	printf("0x%02X\n", SPI0.DATA);
}

//
void SPI_0_write(uint8_t data)
{
	SPI0.DATA = data;
}

void SPI_0_init(void (*f_in)(uint8_t))
{
	// Define callback function
	CallbackFunc = f_in;

	SPI0.CTRLA = 0 << SPI_CLK2X_bp    /* Enable Double Speed: disabled */
	             | 0 << SPI_DORD_bp   /* Data Order Setting: disabled */
	             | 1 << SPI_ENABLE_bp /* Enable Module: enabled */
	             | 0 << SPI_MASTER_bp /* SPI module in SLAVE mode */
	             | SPI_PRESC_DIV4_gc; /* System Clock / 4 */

	 SPI0.CTRLB = 0 << SPI_BUFEN_bp /* Buffer Mode Enable: disabled */
			 | 0 << SPI_BUFWR_bp /* Buffer Write Mode: disabled */
			 | SPI_MODE_0_gc /* SPI Mode 0 */
			 | 0 << SPI_SSD_bp; /* Slave Select Disable: disabled */

	 SPI0.INTCTRL = 0 << SPI_DREIE_bp /* Data Register Empty Interrupt Enable: disabled */
			 | 1 << SPI_IE_bp /* Interrupt Enable: enable */
			 | 0 << SPI_RXCIE_bp /* Receive Complete Interrupt Enable: disabled */
			 | 0 << SPI_SSIE_bp /* Slave Select Trigger Interrupt Enable: disabled */
			 | 0 << SPI_TXCIE_bp; /* Transfer Complete Interrupt Enable: disabled */

	SPI_0_desc.status = SPI_FREE;
}

void SPI_0_enable()
{
	SPI0.CTRLA |= SPI_ENABLE_bm;
}

void SPI_0_disable()
{
	SPI0.CTRLA &= ~SPI_ENABLE_bm;
}

uint8_t SPI_0_exchange_byte(uint8_t data)
{
	// Blocking wait for SPI free makes the function work
	// seamlessly also with IRQ drivers.
	while (SPI_0_desc.status == SPI_BUSY)
		;
	SPI0.DATA = data;
	while (!(SPI0.INTFLAGS & SPI_RXCIF_bm))
		;
	return SPI0.DATA;
}

void SPI_0_exchange_block(void *block, uint8_t size)
{
	uint8_t *b = (uint8_t *)block;
	while (size--) {
		SPI0.DATA = *b;
		while (!(SPI0.INTFLAGS & SPI_RXCIF_bm))
			;
		*b = SPI0.DATA;
		b++;
	}
}

void SPI_0_write_block(void *block, uint8_t size)
{
	uint8_t *b = (uint8_t *)block;
	while (size--) {
		SPI0.DATA = *b;
		while (!(SPI0.INTFLAGS & SPI_RXCIF_bm))
			;
		b++;
	}
}

void SPI_0_read_block(void *block, uint8_t size)
{
	uint8_t *b = (uint8_t *)block;
	while (size--) {
		SPI0.DATA = 0;
		while (!(SPI0.INTFLAGS & SPI_RXCIF_bm))
			;
		*b = SPI0.DATA;
		b++;
	}
}
