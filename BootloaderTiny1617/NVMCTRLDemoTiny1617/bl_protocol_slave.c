#include <atomic.h>
#include <avr/pgmspace.h>
#include "bl_protocol_slave.h"
#include "clock_config.h"

// Locals
uint32_t wr_addr;
uint32_t rd_addr;	
uint8_t cmd;
uint8_t input_buff[6];
uint8_t addr_buff = 0;
volatile uint8_t input_cnt = 0;
volatile uint8_t ready_f = 1;

uint8_t ram_buff[PROGMEM_PAGE_SIZE];


//--------------------------------------------------
// Chip Select (/CS) release event interrupt
//--------------------------------------------------
ISR( PORTA_PORT_vect )
{
	// Clr interrupt flag
	PORTA.INTFLAGS = PORTA.INTFLAGS;

	// If Write command ends
	if ((cmd == BL_CMD_WRITE) && (addr_buff))
	{
		// Mark 'Busy'
		ready_f = 0;		
	}
				
	input_cnt = 0;		
	addr_buff = 0;

}

//--------------------------------------------------
// Bootloader Commands Parser
//--------------------------------------------------
void bSlave_ISR(uint8_t data_in)
{	
    // Add new start data (cmd + addr)
    if (input_cnt <= 5)
    {
        input_buff[input_cnt++] = data_in;
    }
    // Get command
    cmd = input_buff[0];

    switch (cmd)
    {
        case BL_CMD_WRITE:
			if (!ready_f) 
			{
				break;
			}
            if (input_cnt == 5)
            {
                wr_addr   = input_buff[1];
                wr_addr <<= 8;
                wr_addr  |= input_buff[2];
                wr_addr <<= 8; 
                wr_addr  |= input_buff[3];
                wr_addr <<= 8;
                wr_addr  |= input_buff[4];
            }
            if (input_cnt > 5) 
            {				
                ram_buff[addr_buff % PROGMEM_PAGE_SIZE] = data_in;				
				addr_buff++;				 
				
            }
            break;
 
        case BL_CMD_READ:
			if (!ready_f)
			{
				break;
			}		
            if (input_cnt == 5)
            {
                rd_addr   = input_buff[1];
                rd_addr <<= 8;
                rd_addr  |= input_buff[2];
                rd_addr <<= 8;
                rd_addr  |= input_buff[3];
                rd_addr <<= 8;
                rd_addr  |= input_buff[4];				
            }
            if (input_cnt >= 5)
            {				
				SPI_0_write(FLASH_0_read_flash_byte(rd_addr));
				rd_addr++;
            }
            break;

        case BL_CMD_STATUSR:
            if (input_cnt == 1)
            {
                SPI_0_write(BL_ID_MASK | (ready_f << BL_SR_RDY));
            }
            break;

        case BL_CMD_RESET:
            if (input_cnt == 2) 
            {
                if (data_in == BL_CMD_RESET)
                {
					//__disable_interrupt();
                    // Start to Reset MCU Now!
					RSTCTRL_reset();  								
					//RSTCTRL_call_application();		
					
					// Never return, this saves code space since stack
					// need not be preserved on function entry.
					while (1);								
                }
            }
            break;

        case BL_CMD_SET_UPDRDY_FLAG:
			if (input_cnt == 2)
			{
				if (data_in == BL_CMD_SET_UPDRDY_FLAG)
				{
					// Setup Update ready flag
					FLASH_0_write_eeprom_byte(BL_APP_BOOTFLAG_ADDR + 0, BL_APP_BOOTFLAG_MAGICWORD);											        
				}
			}
			break;
		
        case BL_CMD_CLR_UPDRDY_FLAG:
			if (input_cnt == 2)
			{
				if (data_in == BL_CMD_SET_UPDRDY_FLAG)
				{
					// Setup Update ready flag
					FLASH_0_write_eeprom_byte(BL_APP_BOOTFLAG_ADDR + 0, 0xFF);
				}
			}
			break;

        
        case BL_CMD_GET_VERSION:
			if (input_cnt == 1)
			{
				SPI_0_write(0x34);
			}
			break;
        
        case BL_CMD_ISBOOTLOADER:
			if (input_cnt == 1)
			{
				SPI_0_write(0x01); //YES bootloader (it is bootloder)
			}
			break;


		
        default:
            input_cnt = 0;
			addr_buff = 0;
    }	

}

//--------------------------------------------------
// Bootloader Init function
//--------------------------------------------------
void bSlave_Init(void)
{
	SPI_0_init(bSlave_ISR);
}

//--------------------------------------------------
// Main Bootloader routine function
//--------------------------------------------------
void bSlave_Main(void)
{
	volatile nvmctrl_status_t status;	
		 
	// Read value of BOOTEND and APPEND from FUSE section of memory map
	uint8_t bootend = FUSE.BOOTEND;	
	if (bootend < (BL_BOOT_MIN_SIZE / 256)) 
	{ 
		//printf("BOOT section too small program, set BOOTEND fuse and retry\r\n");
		while(1);		
	}
/*	
	uint8_t append = FUSE.APPEND;
	if (append < ((BL_APP_MIN_SIZE + BL_BOOT_MIN_SIZE) / 256))
	{
		//printf("Application section too small program, set APPEND fuse and retry\r\n");
		while(1);
	}	
*/

    // Test illegal write, BOOT should not be allowed to write to BOOT
    status = FLASH_0_write_flash_byte(0, NULL, 1);
	if (status != NVM_ERROR) 
	{
		//printf("Attempted illegal write from BOOT to BOOT\r\n");
	    while(1);
	}
	

    // Read the first location of application section
    // which contains the address of stack pointer
    // If it is 0xFFFFFFFF then the application section is empty
	// Start Application if 'Magic word exist'
    if ((FLASH_0_read_flash_byte(FUSE.BOOTEND * 256) != 0xFF)  && 
		(FLASH_0_read_eeprom_byte(BL_APP_BOOTFLAG_ADDR + 0) == BL_APP_BOOTFLAG_MAGICWORD) ) {				
					
		// Start main App
	    RSTCTRL_call_application(); 
		
        // Never return, this saves code space since stack
        // need not be preserved on function entry.
        while (1);		
    }  


	while(1)
	{ 
		// Begin to Start to write data
		if (!ready_f)
		{
			FLASH_0_erase_flash_page(wr_addr);
			FLASH_0_write_flash_page(wr_addr, ram_buff);
						
			// Release Ready flag
			ready_f = 1;
		}		
	}
}
