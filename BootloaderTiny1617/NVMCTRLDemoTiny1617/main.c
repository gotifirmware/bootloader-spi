#include <atmel_start.h>
#include <stdio.h>
#include "bl_protocol_slave.h"
 
/*
NOTE:
Before executing this program, make sure the BOOTEND fuse is set so that the
BOOT section is large enough to hold this program, but small enough to leave
room for the APPLICATION section which is programmed by this code.
The reason for this is stated in the tiny1617 datasheet under
"Inter-Section Write Protection":
 * Code in the BOOT section can write to APPCODE and APPDATA.

Note that the INTCTRL in the START project has been configured to move
INTVEC to start of BOOT section. Setting BOOTEND to something different from 0
causes start of APPCODE to be different from 0.
If we didn't relocate the INTVEC by writing IVSEL, we would have
to use the linker to place the ISR at start of APPCODE.
*/
 
int main(void) 
{ 
    if ((FLASH_0_read_flash_byte(FUSE.BOOTEND * 256) != 0xFF)  &&
    (FLASH_0_read_eeprom_byte(BL_APP_BOOTFLAG_ADDR + 0) == BL_APP_BOOTFLAG_MAGICWORD) ) 
	{
			asm ("jmp 0x1400");
			return 0;
	}

	
	// Initializes MCU, drivers and middleware
	atmel_start_init(); 
	// Start MCU at max Frequency (20Mhz)
	CCP = CCP_IOREG_gc;
	CLKCTRL_MCLKCTRLB = 0x00;	
	// Enable Interrupts
	sei(); 

	// Bootloader init
	bSlave_Init();
	
	// Main bootloader routine
	bSlave_Main();
}
