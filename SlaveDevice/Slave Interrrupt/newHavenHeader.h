
#ifndef HAVEN_DISPLAY_H
#define HAVEN_DISPLAY_H


////////////////////////////////////////////////
//
//	Name:
//			newHavenHeader
//
////////////////////////////////////////////////
#include <atmel_start.h>

// --- DEFINES ---
// ---  bit-banged Display  ---
// ---               SPI:  Slave Select ---
#define  ENABLE_LOW			DS_set_level( false);   // --- was PA1 ---
#define  ENABLE_HIGH		DS_set_level( true);
// ---               SPI: Master Clock ---
#define  CLOCK_LOW			DC_set_level(false);   // --- was PB2 ---
#define  CLOCK_HIGH			DC_set_level(true);
// ---               SPI: MOSI ---
#define  OUTPUT_LOW			DM_set_level( false);   // --- was PA2 ---
#define  OUTPUT_HIGH		DM_set_level( true);




// --- FUNCTION PROTOTYPES ---
void initDisplay(void);
void writeCommand(uint8_t command );
void writeDataPrefix(void);
void writeData(uint8_t data);
void sendToDisplay(uint16_t number, uint8_t address, bool symbol, int8_t offset);
void displayHexByte(uint8_t number, uint8_t address);


#endif // --- HAVEN_DISPLAY_H ---

// --- EOF ---