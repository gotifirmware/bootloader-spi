
/////////////////////////////////////////////////////////////////////
//
//	Name:
//			newHavenDisplay.c
//
//	Purpose:
//			Real time data monitor
//
//	Details:
//			bit-banged SPI interface
//
//	Author:
//			Valentin Ivanov
//
////////////////////////////////////////////////////////////////////

#include "newHavenHeader.h"

#define   DELAY     for( int delcho = 0; delcho < 50; delcho++);
#define   LONGWAIT	 for( int b = 0; b< 32000; b++);

// === B O D Y ===

//////////////////////////////////////////////////
//
//	Name:
//		init
//
//	Purpose:
//		Initializes the display controller
//
//////////////////////////////////////////////////
void initDisplay()
{
	CLOCK_LOW
	writeCommand(0x38);	// --- Function Set
	writeCommand(0x08);	// --- Display OFF
	writeCommand(0x01);	// --- Display Clear
	writeCommand(0x06);	// --- Entry Mode Set
	writeCommand(0x02);	// --- Home Command
	writeCommand(0x0C);	// --- Display ON
	writeCommand(0x80);	// --- DRAM Initial Address ---
	
} // --- eof init( ) ---



//////////////////////////////////////////////////////////////////////////////////////////
//
//	Name:
//		writeCommand
//
//////////////////////////////////////////////////////////////////////////////////////////
void writeCommand( uint8_t command )
{
	int8_t counter;
	
	// --- Inital states ---
	ENABLE_HIGH
	CLOCK_LOW
	OUTPUT_LOW
	DELAY
	// --- prefix ---
	ENABLE_LOW		// SS  = 0
	CLOCK_LOW		// CLK = 0
	OUTPUT_LOW		// OUT = 0	  // --- RS = 0 (Command)
	DELAY
	CLOCK_HIGH		// CLK = 1	  // --- RS state read
	DELAY
	CLOCK_LOW		// CLK = 0
	DELAY
	// --- RW = 0
	CLOCK_HIGH		// CLK = 1     //--- RW state read
	DELAY
	CLOCK_LOW		// CLK = 0
	DELAY
	
	// --- data ---
	for ( counter = 0; counter < 8; counter++ )
	{
		if( (command & 0x80) == 0x80 ) OUTPUT_HIGH
		else  OUTPUT_LOW
		
		DELAY
		CLOCK_HIGH
		DELAY
		CLOCK_LOW
		
		command = command<<1;
	}
	
	ENABLE_HIGH
	
} // --- eof writeCommand( ) ---


//////////////////////////////////////////////////////////////////
//
//	Name:
//		writeDataPrefix: 2 bits
//
//	Purpose:
//		Initiates data sending to the display controller
//
//	Notes:
//		Only the first Data byte is preceded by RS and RW
//
/////////////////////////////////////////////////////////////////
void writeDataPrefix()
{
	// --- Initial state ---
	ENABLE_HIGH    //: will be driven from the higher level
	CLOCK_LOW
	OUTPUT_LOW
	DELAY
	// --- PROCEED ---
	ENABLE_LOW
	CLOCK_LOW
	OUTPUT_HIGH	// --- RS = 1 (Data)
	DELAY
	CLOCK_HIGH   // --- RS state read
	DELAY
	CLOCK_LOW
	OUTPUT_LOW 	// --- RW = 0
	DELAY
	CLOCK_HIGH	// --- RW state read
	DELAY
	CLOCK_LOW
	DELAY
	
	// --- Don't close the ENABLE, the data byte follows ---

} // --- eof writeDataPrefix( ) ----



//////////////////////////////////////////////////
//
//	Name:
//		writeDataBytes
//
//	Purpose:
//		Writes all the bytes after the first one
//
//////////////////////////////////////////////////
void writeData(uint8_t dataByte)
{
	int8_t counter;
	
	for ( counter = 0; counter < 8; counter++ )
	{
		if( (dataByte & 0x80) == 0x80 ) OUTPUT_HIGH
		else  OUTPUT_LOW
		
		DELAY
		CLOCK_HIGH
		DELAY
		CLOCK_LOW
		
		dataByte = dataByte<<1;
	}
} // --- eof writeData( ) ---



//////////////////////////////////////////////////////////////////
//
//	Name:
//		sendToDisplay
//
/////////////////////////////////////////////////////////////////
//void sendToDisplay(uint16_t number, uint8_t address, bool symbol)
void sendToDisplay(uint16_t number, uint8_t address, bool symbol, int8_t offset)
{
	uint8_t		firstDigit, secondDigit, thirdDigit, fourthDigit, fifthDigit;
	
	if(symbol)
	{
		writeCommand(address);
		writeDataPrefix();
		writeData(number + offset);
		ENABLE_HIGH
		return;
	}
	
	
	firstDigit = number/10000;
	number = number- firstDigit*10000;
	secondDigit = number/1000;
	number = number- secondDigit*1000;
	thirdDigit = number /100;
	number = number- thirdDigit*100;
	fourthDigit = number/10;
	number = number- fourthDigit*10;
	fifthDigit = number;
	
	
	writeCommand(address);
	writeDataPrefix();
	
	if(firstDigit != 0)
	{
		writeData(firstDigit  + 48);
		writeData(secondDigit + 48);
		writeData(thirdDigit  + 48);
		writeData(fourthDigit + 48);
		writeData(fifthDigit  + 48);
	}
	else
	{
		writeData(0x20);
		if(secondDigit != 0)
		{
			writeData(secondDigit + 48);
			writeData(thirdDigit  + 48);
			writeData(fourthDigit + 48);
			writeData(fifthDigit  + 48);
		}
		else
		{
			writeData(0x20);
			if(thirdDigit != 0)
			{
				writeData(thirdDigit  + 48);
				writeData(fourthDigit + 48);
				writeData(fifthDigit  + 48);
			}
			else
			{
				writeData(0x20);
				if(fourthDigit != 0)
				{
					writeData(fourthDigit + 48);
					writeData(fifthDigit  + 48);
				}
				else
				{
					writeData(0x20);
					writeData(fifthDigit  + 48);
				}
			} // third
		} // second
	} // first
	
	ENABLE_HIGH
}


//////////////////////////////////////////////////////////////////
//
//	Name:
//		displayHexByte
//
//////////////////////////////////////////////////////////////////
void displayHexByte(uint8_t number, uint8_t address)
{
	uint8_t		upperNibble, lowerNibble;
	uint8_t		offset;
	
	upperNibble = number/16;
	lowerNibble = number - ( 16*upperNibble);
	
	writeCommand(address);
	writeDataPrefix();
	
	// --- make it a function call in case of more digits ---
	if( upperNibble < 10 ) offset =	0x30;
	else offset = 0x37;
	writeData(upperNibble  + offset);
	
	if( lowerNibble < 10 ) offset =	0x30;
	else offset = 0x37;
	writeData(lowerNibble + offset);
	
	ENABLE_HIGH
}

