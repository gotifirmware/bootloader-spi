
//////////////////////////////////////////////////////////////////////////////////
//
//		Hayward with Interrupt-SPI
//
//		C:\GUY Oct 16\Slave Interrupt\Slave Interrrupt.cproj
//
//      size:
//
//		Note:
//         There will be no SLAVE DISPLAY support .
//
//		Note:
//			The dummy byte sent by Master has to be 0x1E .
//
//		Note:
//			Now we are running Protocol v. 8.0
// 
/////////////////////////////////////////////////////////////////////////////////

// --- INCLUDES ---
#include <atmel_start.h>  
#include <touch.h>
#include <atomic.h>
#include <hayward.h>
#include <haySquare.h>
//#include <stdlib.h>  // for abs();
#include <util/delay.h>

#include <nvmctrl_basic.h>
#include <bl_protocol_slave.h>

// --- DEFINES ---
#define		NO_MAIL_PENDING			0
#define		MAIL_RECEIVED			1
#define		MAIL_TO_SEND			2
#define		SLEEP					2000000		// --- 2,000,000 ---  not used now!
//#define		VALUE_MAGNIFIER				 25
//#define		LOWEST_REVOLUTIONS		    600
//#define		HIGHEST_REVOLUTIONS        3450
#define		BIT_BANGED_I2C


// --- tempo, for testing ---
#define		VALUE_MAGNIFIER				  5
#define		LOWEST_REVOLUTIONS		     25
#define		HIGHEST_REVOLUTIONS         225

// Read/EWrite modes
#define		MODE_NONE				0
#define		MODE_WRITE				1
#define		MODE_READ				2


// --- GLOBAL DATA ---
uint8_t		Cassidento[1];	
uint8_t	    symbolToSend[1];
uint8_t		symbolReceived;
volatile static int8_t		MODE = MODE_NONE;
uint8_t		receiveBuffer[2];
int8_t      receiveBufferCounter, transmitBufferCounter;
int8_t	    status;	// --- initialized to <0> as global ---
bool        swipeTrigger;
int8_t		numberOfSwipes;
bool		flagBeginSwipe;
int16_t              difference;
//int8_t				shiftReg[2];
//uint8_t				positionCarrier, touchMemory;
volatile uint8_t	scroller_status;
bool                direction;
bool				goSpinnerUP, goSpinnerDOWN;
int16_t             valueToDisplay;
uint8_t				blinkState;
uint32_t			timerVariable;
int16_t				swipeRateCounter;
uint32_t			blinkTimerSetValue;
uint32_t			blinkTimerVariable;

volatile  uint8_t	        sPosition;
volatile  uint8_t			positionCarrier, touchMemory;
int8_t			            shiftReg[2];
bool				statusStateTracker[2];
bool				enableTimer;

bool				isKeyTouchedNow[MAX_NUM_PADS];
int32_t				longTouchTimerVariable;
bool                padError;
int8_t				keyNode;
int32_t				trackingCounter;
bool				longTouchCounterIsRunning;
int8_t			mapA[] = {  0,  0,  0,  2,  1,  4,  8,  1,  2,  4, 16, 32, 64 };
int8_t		    mapB[] = {  0,  0,  0,  1,  1,  1,  1,  2,  2,  2,  1,  1,  1 };
int8_t			keyPad[MAX_NUM_PADS];	
uint8_t			newState[MAX_NUM_PADS]    = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
uint8_t			stateMemory[MAX_NUM_PADS] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
uint8_t			lastReading;
uint8_t		    previousReading;
bool            lockINT;


volatile static uint8_t	spi_regdata = 0;
volatile static uint8_t	spi_read_f = 0;
volatile static uint8_t	spi_regaddr = 0;
volatile static uint8_t	spi_shift_reg = SPI_WR_SIGNATURE;

// --- FUNCTION PROTOTYPES ---
void myFunction(uint8_t data_in);


// --- BODY ---

////////////////////////////////////////////////////////////
// Chip Select (/CS) release event interrupt
////////////////////////////////////////////////////////////
ISR( PORTA_PORT_vect )
{
	// Clr interrupt flag
	PORTA.INTFLAGS = PORTA.INTFLAGS;
}


////////////////////////////////////////////////////////////
// BOOT INTO BOOTLOADER
// Return Error code in no possible to start bootloader
////////////////////////////////////////////////////////////
uint8_t BootloaderStart(void)
{
	// Check bootloder exist
    if (FLASH_0_read_flash_byte(FUSE.BOOTEND * 256) == 0xFF)  return 1; 
	  
	// Clear magic number 
	FLASH_0_write_eeprom_byte(BL_APP_BOOTFLAG_ADDR + 0, 0xFF); 
	_delay_ms(100);
				
	// Do hardware reset
	DISABLE_INTERRUPTS();
	// Start to Reset MCU Now!
	RSTCTRL_reset();
	// Never return, this saves code space since stack
	// need not be preserved on function entry.
	while (1);	
	
	return 0;
}
	
	
////////////////////////////////////////////////////////////
//
//	Implement the SPI communication here
//
/////////////////////////////////////////////////////////////
void myFunction(uint8_t data_in)
{
 // --- BODY ---	       
    if (MODE == MODE_NONE)
	{	
		if (data_in & 0x80)
		{		
			MODE = MODE_READ;			// --- READ ---
		} 
		else
		{
			MODE = MODE_WRITE;			       // --- WRITE ---
		}	  
		// Get addr
		spi_regaddr = data_in & 0x7F;
		if (spi_regaddr >= REGISTERS_AMOUNT_N) spi_regaddr = 0;
	  
		if (MODE == MODE_READ)	  
		{		  
			SPI_0_write(spi_shift_reg);		  
			spi_shift_reg = commandsRegistry[spi_regaddr];
		} 
		else
		{
			SPI_0_write(SPI_WR_SIGNATURE);			
		}		  	
		return;
	}
   
   
	if (MODE == MODE_READ)      // --- READ ---
	{
		// Continue read?
	    if ((data_in & 0x80) || (data_in == SPI_DUMMY_BYTE)) 
		{
			SPI_0_write(spi_shift_reg);
			spi_regaddr = data_in & 0x7F;
			if (spi_regaddr >= REGISTERS_AMOUNT_N) spi_regaddr = 0;			
			spi_shift_reg = commandsRegistry[spi_regaddr];
			if (data_in == SPI_DUMMY_BYTE) spi_shift_reg = SPI_WR_SIGNATURE;			
		}
		else
		{		 	
			MODE = MODE_NONE;  // --- end of READ sequence ---			
			SPI_0_write(SPI_WR_SIGNATURE);
	    }
	}
	else // MODE_WRITE
	{	
		SPI_0_write(SPI_WR_SIGNATURE);
		MODE = MODE_NONE;  // --- end of WRITE sequence ---
		
		// Check registers writable
		if ((spi_regaddr == REG_STATUS) ||
		    (spi_regaddr == REG_CONTROL) || 
			((spi_regaddr >= REG_COUNTER_LOW) && (spi_regaddr <= REG_BLINKRATE)))	
		{
			commandsRegistry[spi_regaddr] = data_in;	
			spi_regdata = data_in;
			spi_read_f = 1;
		}
	} // ---eof else	 
	
} // --- eof callback function ---


/////////////////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////////////////
void wordToTwoRegisters(int8_t node, int8_t regHigher, int8_t regLower)
{
 uint16_t value;
 
	value = get_sensor_node_signal(node);
	commandsRegistry[regLower]  = (uint8_t)( value & 0x00FF);
	commandsRegistry[regHigher] = (uint8_t)(( value & 0xFF00)>>8);
}


////////////////////////////////////////////////////////////////
//
//	main
//
///////////////////////////////////////////////////////////////
int main(void)
{
	// --- local variables ---  
	uint8_t		speed_1_key;
	uint8_t		speed_2_key;
	uint8_t		speed_3_key;
	uint8_t		speed_4_key;
	uint8_t		stop_resume_key;
	uint8_t		quick_clean_key;
	uint8_t		menu_key;
	uint8_t		left_arrorw_key;
	uint8_t		right_arrow_key;
	int8_t		step;
	bool	    three;
	uint8_t     buttonsLock;
	bool		flagRegularKeyTouched;
	int8_t		swipeSample_1, swipeSample_2, diff;
	bool		lockKeypad; 
	bool        longTouchShifter[2];
	bool		touchShifter[2];
	int8_t      counter, index, adjustment, effective_pads;
	uint8_t		startReading, endReading;
	uint16_t	guardKeyValue, sectorOneValue, sectorTwoValue, sectorThreeValue;	
	
// ------------------ B O D Y -----------------------

	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	//Enable_global_interrupt();  // --- Global Interrupt Enable
	ENABLE_INTERRUPTS();
	//SREG |= 0x80;				// --- Enable interrupts
	
	SPI_0_init(myFunction);
	SPI_0_enable();
	//spi_transfer_done_cb_t myFunctionPointer = &myFunction;
	//SPI_0_register_callback(myFunctionPointer);
		
    // --- configure the LED Controller here -------------------------
	// --- CLK ---
	PORTB_set_pin_pull_mode(3, PORT_PULL_OFF);
	PORTB_set_pin_dir(3, PORT_DIR_OUT);
	PORTB_set_pin_level(3, true);
	// --- SDA ---
	PORTB_set_pin_pull_mode(2, PORT_PULL_OFF);
	PORTB_set_pin_dir(2, PORT_DIR_OUT);
	PORTB_set_pin_level(2, true);
	
	/* --- transfer clock for the new LCD controller ---
	PORTB_set_pin_pull_mode(6, PORT_PULL_OFF);
	PORTB_set_pin_dir(6, PORT_DIR_OUT);
	PORTB_set_pin_level(6, true);
    */
    i2cConfig();
  	// ----------------------------------------------------------------
	  
	PORTB_set_pin_pull_mode(7, PORT_PULL_OFF);
	PORTB_set_pin_dir(7, PORT_DIR_OUT);
	PORTB_set_pin_level(7, false);	// --- set the INT line to "0" --- 
	  
	flagRunState = 0x01;	// --- we hardcode the Run State to <App> temporarilly;
	flagDisableKeyScans = false;
	timerVariable = BLINK_CONSTANT;
	blinkState = 0;
	//valueToDisplay			= 600;
	valueToDisplay		 =      5;  // --- temporarily ---
	goSpinnerUP				= true;
	goSpinnerDOWN			= true;
	flagRegularKeyTouched	= false;
	flagLongKeyTouched		= false;  // --- will deal with this one later ---
	flagSwipePadTouched		= false;
	// --- simulation ---
	flagDisableKeyScans = false;
	
	// --- set up the Control Registry --- 
	commandsRegistry[REG_ADDR_BOOT0] = 0xB0;  // ----Bootloader Revision---
	commandsRegistry[REG_ADDR_BOOT1] = 0xB1;
	commandsRegistry[REG_ADDR_BOOT2] = 0xB2;
	commandsRegistry[REG_ADDR_BOOT3] = 0xB3;
	commandsRegistry[REG_ADDR_BOOT4] = 0xB4;
	
	commandsRegistry[REG_ADDR_APP0] = 0xA0;  // --- Application Revisions ---
	commandsRegistry[REG_ADDR_APP1] = 0xA1;
	commandsRegistry[REG_ADDR_APP2] = 0xA2;
	commandsRegistry[REG_ADDR_APP3] = 0xA3;
	commandsRegistry[REG_ADDR_APP4] = 0xA4;
	
	commandsRegistry[REG_STATUS] = 0x80;  // --- according to 5.1.3 ---
	commandsRegistry[REG_CONTROL] = 0x01;  // --- according to 5.1.4 ---
	
	commandsRegistry[REG_KEYPAD1] = 0x02;  // --- according to 5.1.5 ---
	commandsRegistry[REG_KEYPAD2] = 0x03;  // --- according to 5.1.6 ---
	commandsRegistry[REG_WHEEL_LAST] = 0x04;  // --- according to 5.1.7 ---
	commandsRegistry[REG_WHEEL_PREV] = 0x32;  // --- according to 5.1.8 ---  // --- read the wheel within 500 mS ---
	commandsRegistry[REG_NOTUSED_16] = 0x05;  // --- according to 5.1.9 ---
	commandsRegistry[REG_NOTUSED_17] = 0x06;  // --- according to 5.1.10 ---

	commandsRegistry[REG_COUNTER_LOW] = 0x07;  // 
	commandsRegistry[REG_COUNTER_HIGH] = 0x08;  // 

	commandsRegistry[REG_LED] = 0x09;  // --- according to 5.1.12 ---	
	commandsRegistry[REG_BLINK] = 0x0A;  // --- according to 5.1.12 ---	
	commandsRegistry[REG_BLINKRATE] = 0x64;  // --- according to 5.1.11 ---  // --- 100 units of 50 mS each = 5 s ---
	
	commandsRegistry[REG_WHEEL_S0_LOW] = 0x0B;  // --- according to 5.1.12 ---	
	commandsRegistry[REG_WHEEL_S0_HIGH] = 0x0C;  // --- according to 5.1.12 ---	

	commandsRegistry[REG_WHEEL_S1_LOW] = 0x0D;  // --- according to 5.1.12 ---
	commandsRegistry[REG_WHEEL_S1_HIGH] = 0x0E;  // --- according to 5.1.12 ---

	commandsRegistry[REG_WHEEL_S2_LOW] = 0x0F;  // --- according to 5.1.12 ---
	commandsRegistry[REG_WHEEL_S2_HIGH] = 0x11;  // --- according to 5.1.12 ---
	
	commandsRegistry[REG_NOTUSED_29] = 0x12;  //
	
	commandsRegistry[REG_SIGNATURE] = SPI_WR_SIGNATURE; 
		
	//MODE = MODE_NONE;
	//SPI_0_read_block(Cassidento, 1);
	
	swipeTrigger = false;
	swipeRateCounter = SWIPE_RATE_COUNTER;
	numberOfSwipes = 0;
	flagBeginSwipe = false;
	blinkTimerVariable	= BLINK_CONSTANT;
	blinkTimerSetValue  = BLINK_CONSTANT;
	touchMemory	 = 0;	
	shiftReg[1] = 0;
	
					
while (1)
{
	 		 
// ================ CAPACITIVE TOUCH SECTION ===================================================	
      touch_process();
		 
// =========================  P A D S   P R O C E S S I N G  ==================================
   
     if( (commandsRegistry[REG_CONTROL] & 0x20 ) == 0x20 )  // --- Guard disabled ---
	 { 
		  adjustment = -1;
		 
	 }
	 else                                         // --- Guard enabled ---
	 {
		  adjustment = 0;
		  wordToTwoRegisters(12, 16, 17);
						// guardKeyValue = get_sensor_node_signal(12);
						//commandsRegistry[xx16] = (uint8_t)(guardKeyValue & 0x00FF);
						//commandsRegistry[xx17] = (uint8_t)((guardKeyValue & 0xFF00)>>8);
		
	 }
	 
	 effective_pads = MAX_NUM_PADS + adjustment;  // --- with/without the Guard Key ---
	                                                   
// --- read the pads ---
for( counter = 0; counter < effective_pads; counter++)
{
	index = counter + PAD_NODE_OFFSET;
	newState[counter] = get_sensor_state( index ) &  0x80;
	if( newState[counter] == stateMemory[counter] ) continue;
	
   if( (get_sensor_state( index ) &  0x80 ) == 128)
   {
	 if(mapB[index] == 1) commandsRegistry[REG_KEYPAD1] |= mapA[index];
	 if(mapB[index] == 2) commandsRegistry[REG_KEYPAD2] |= mapA[index];
	 commandsRegistry[REG_STATUS] |= 0x0C;  
	 // --- raise INT ---
	 IRQ_RAISED
	           
   } // ---det_sen ...
   else
   {
	if(mapB[index] == 1) commandsRegistry[REG_KEYPAD1] &= ~mapA[index];
	if(mapB[index] == 2) commandsRegistry[REG_KEYPAD2] &= ~mapA[index];
	commandsRegistry[REG_STATUS] |= 0x0C;
	// --- raise INT ---
	IRQ_RAISED  
   }
     
	if( (get_sensor_state(12) &  0x80 ) == 128) driveLEDs(0x01);
	else  driveLEDs(0x00);
	 
	 
   stateMemory[counter] = newState[counter];
   
} // --- for ...

		// --- test for the new LCD controller ---
		//  if(! PORTB_get_pin_level(5) ) sendByteNew(0x55);


// ================  W H E E L  P R O C E S S I N G ===========================

  	scroller_status		= get_scroller_state(0); // --- usually reads 0x81 ---
			
	if( scroller_status )	
	{
		lastReading = 	(uint8_t)get_scroller_position(0);  
		
		if(lastReading != previousReading )
		{
			commandsRegistry[REG_WHEEL_LAST] = lastReading;
			// --- three sectors stored ---
			wordToTwoRegisters(0, 23, 24);
			wordToTwoRegisters(1, 25, 26);
			wordToTwoRegisters(2, 27, 28);
			        /*
					sectorOneValue = get_sensor_node_signal(0);
					commandsRegistry[REG_WHEEL_S0_HIGH] = (uint8_t)( sectorOneValue & 0x00FF);
					commandsRegistry[REG_WHEEL_S0_LOW] = (uint8_t)(( sectorOneValue & 0xFF00)>>8);
						sectorTwoValue = get_sensor_node_signal(1);
						commandsRegistry[REG_WHEEL_S1_HIGH] = (uint8_t)(sectorTwoValue & 0x00FF);
						commandsRegistry[REG_WHEEL_S1_LOW] = (uint8_t)((sectorTwoValue & 0xFF00)>>8);
							sectorThreeValue = get_sensor_node_signal(2);
							commandsRegistry[REG_WHEEL_S2_HIGH] = (uint8_t)(sectorThreeValue & 0x00FF);
							commandsRegistry[REG_WHEEL_S2_LOW] = (uint8_t)((sectorThreeValue & 0xFF00)>>8);
					*/		
			commandsRegistry[REG_WHEEL_PREV] = previousReading;
			// --- three sectors stored ---- ? we need three more registers ?
			IRQ_RAISED
		    commandsRegistry[REG_STATUS] |= 0x09;  // --- 0 0 0 0  1 0 0 1
			previousReading = lastReading;
			lockINT = true;
		}
	}
	else
	{
		if(lockINT == true)
		{
		 IRQ_CLEARED
		 commandsRegistry[REG_STATUS] &= 0xF6;   // --- 1 1 1 1  0 1 1 0 ---
		 previousReading = 0;
		 lockINT = false;
		}
	}
	
	
// --- eof wheel ....		
	
			
// ===================  I2C LEDs CONTROL SECTION  ===============================
		blinkTimerVariable--;
		if( blinkTimerVariable == 0 )
		{
		 makeBlink();
		 blinkTimerVariable = 100 * commandsRegistry[REG_BLINKRATE];
		}
// --- eof i2c -----


		// After WRITE analyis
		if (spi_read_f)		
		{
			spi_read_f = 0;
			switch (spi_regaddr)
			{
				case 10: break;
				case 11: 
					// Module Control Register
					// Enter Bootloader command
					if (spi_regdata & 0x08) BootloaderStart(); 
					break;
				
				case 18: break;							
				case 19: break;		
					
				case 20: 
					// Some LED operation ?
					if( spi_regaddr == 20) driveLEDs(spi_regdata);				
					break;			
					
				case 21: break;
				case 22: break;
				case 23: break;
				case 24: break;
			}			
		}	
	  	   
    }// --- while ---
	
}
// --- EOF ----




/*
// ----- < NUMBER OF WHEEL SWIPES MONITOR > -----
if(scroller_status > 0)
{
	flagBeginSwipe = true;
	commandsRegistry[REG_STATUS] |= MASK_ZERO;
}
if( (scroller_status == 0) && flagBeginSwipe )
{
	numberOfSwipes++;
	flagBeginSwipe = false;
	commandsRegistry[xxx19] = numberOfSwipes;
}
if( (commandsRegistry[REG_STATUS] & 0x01) == 0 )
{
	numberOfSwipes = 0;
	commandsRegistry[xxx19] = 0;
}
// --- end of MONITOR ---


// ----- WHEEL SWIPE RATE DETERMINATION -----
if( (statusStateTracker[1] == false ) && ( statusStateTracker[0] == true) )
{
	swipeSample_1 = (uint8_t)valueToDisplay;
	swipeRateCounter = SWIPE_RATE_COUNTER;
	enableTimer = true;
	commandsRegistry[xxx14] = 0;  // --- temporary, to be removed ---
	PORTB_set_pin_level(7, true);
	
}

if(enableTimer) swipeRateCounter--;      // --- set to 700 ---

if( swipeRateCounter == 0 )
{
	swipeSample_2 = (uint8_t)valueToDisplay;
	diff = abs( swipeSample_1 - swipeSample_2 );
	// diff >>= 1;						     // --- 5 bits range ---
	// commandsRegistry[xxx14] &= 0x00;       // --- clean space fpr 5 bits --
	commandsRegistry[xxx14] = diff & 0x7F;	     // --- add it to reg. 14 ---
	enableTimer = false;
	
	PORTB_set_pin_level(7, false);
	
	//commandsRegistry[xxx33] = swipeSample_1;   // --- 0x21 ---
	//commandsRegistry[xxx34] = swipeSample_2;   // --- 0x22
	//diff = 0;
}

// --- contingency ---
if( (swipeRateCounter != 0) && (scroller_status == 0) )
{
	diff = 0;
	enableTimer = false;
	//commandsRegistry[xxx14] &= 0xE0;
}

statusStateTracker[1] = statusStateTracker[0];

// --- eof RATE DETERMINATION ---


*/

/* --- old wheel processing ---
// ================  W H E E L  P R O C E S S I N G ===========================

scroller_status		= get_scroller_state(0); // --- usually reads 0x81 ---
if(scroller_status != 0) statusStateTracker[0] = true;
else statusStateTracker[0] = false;

if( (statusStateTracker[0] == true) && (statusStateTracker[1] == false) )
{
	startReading = 	(uint8_t)get_scroller_position(0);
	commandsRegistry[xxx14] = startReading;
	IRQ_RAISED
	commandsRegistry[REG_STATUS] |= 0x09;
}
if( (statusStateTracker[0] == false) && (statusStateTracker[1] == true) )
{
	endReading = (uint8_t)get_scroller_position(0);
	commandsRegistry[xxx15] = endReading;
	IRQ_RAISED
	commandsRegistry[REG_STATUS] |= 0x09;
}

statusStateTracker[1] = statusStateTracker[0];

// --- eof wheel ....
*/







