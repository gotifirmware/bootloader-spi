
#ifndef HAYWARD_H
#define HAYWARD_H

//////////////////////////////////////////////////////
//
//	Name:
//		hayward.h
//
//	Purpose:
//		Describes the Hayward SPI Protocol as implemented
//		on the SLAVE side
//
//	Note:
//		alpha ( under consideration )  
//
//	Author:
//		Valentin Ivanov
//
/////////////////////////////////////////////////////

#define  IRQ_RAISED				PORTB_set_pin_level(7, true); 
#define  IRQ_CLEARED			PORTB_set_pin_level(7, false); 
#define  BLINK_CONSTANT			700
#define  BIT_BANGED_I2C
#define	 SWIPE_RATE_COUNTER		16384
#define  MAX_NUM_PADS			10   // --- the wheel operates on 3 sensors in this project
#define  LONG_TOUCH_CONSTANT    40000   // --- 1.28 sec ---
#define  PAD_NODE_OFFSET            3  

// --- FUNCTION PROTOTIPES ---
//void sendModuleAddress(void);
//void sendByte(uint8_t dataRegisterAddress);  // --- it is set to 0x02 above ---
//void sendByte(uint8_t i2cByte);				// --- this will shoot to the Upper LEDs buffer ---
//void sendStopCondition(void);


// --- CONTROL VARIABLES AND FLAGS ---
uint8_t		flagRunState;		// --- 1: App ; 0 : bootloader;  !: not initialized yet;
uint8_t     resetCause;
uint8_t     flagReset;
uint8_t		flagInterrupt;  // --- true: SET;  false: RESET;
uint8_t		flagRegularKeyTouched;
uint8_t		flagLongKeyTouched;
uint8_t		flagSwipePadTouched;
bool		flagDisableKeyScans;
uint8_t		longPressTimerConstant;
uint8_t		numberSwipesSince;

#define		SPI_DUMMY_BYTE				0x1E
#define		SPI_WR_SIGNATURE			0x47 // 'G'
#define		SPI_RD_SIGNATURE			0x48 // 'H'
#define		REGISTERS_AMOUNT_N			31
//  =========  COMMANDS REGISTRY =============
// Reg Addresses
#define		REG_ADDR_BOOT0		0		// R		Bootloader Revision (0 if no bootloader) // --- 5 bytes ---
#define		REG_ADDR_BOOT1		1		//
#define		REG_ADDR_BOOT2		2		//
#define		REG_ADDR_BOOT3		3		//
#define		REG_ADDR_BOOT4		4		//
#define		REG_ADDR_APP0		5		//  R		Application Revision 
#define		REG_ADDR_APP1		6		//  
#define		REG_ADDR_APP2		7		//
#define		REG_ADDR_APP3		8		//
#define		REG_ADDR_APP4		9		//
#define		REG_STATUS			10		// R/W		Module Status
#define		REG_CONTROL			11		// R/W		Module Control
#define		REG_KEYPAD1			12		// R		Keypad One Status
#define		REG_KEYPAD2			13		// R		Keypad Two Status
#define		REG_WHEEL_LAST		14		// R		Wheel (Swipe Pad) Last(i.e., most recent) Position
#define		REG_WHEEL_PREV		15		// R		Wheel (Swipe Pad) Previous Position
#define		REG_NOTUSED_16		16		// R		Not Used
#define		REG_NOTUSED_17		17		// R		Not Used
#define		REG_COUNTER_LOW		18		// R/W		Free Running Counter (10 ms period)		// --- 2 bytes ---
#define		REG_COUNTER_HIGH	19		// R/W
#define		REG_LED				20		// R/W		LED Register
#define		REG_BLINK			21		// R/W		LED Blink Register
#define		REG_BLINKRATE		22		// R/W		LED Blink Rate (10 ms units)
#define		REG_WHEEL_S0_LOW	23		// R/W		Wheel Sector 0 Output		// --- 2 bytes ---
#define		REG_WHEEL_S0_HIGH	24 		// R/W
#define		REG_WHEEL_S1_LOW	25		// R/W		Wheel Sector 1 Output		// --- 2 bytes ---
#define		REG_WHEEL_S1_HIGH	26 		// R/W
#define		REG_WHEEL_S2_LOW	27		// R/W		Wheel Sector 2 Output		// --- 2 bytes ---
#define		REG_WHEEL_S2_HIGH	28 		// R/W
#define		REG_NOTUSED_29		29		// R		Not Used
#define		REG_SIGNATURE		30		// R		Signature byte for the CSIM; always reads 0x47
#define		REG_PACK_PROTOCOL	127		// R/W		Packet Protocol Read / Write Header         


volatile uint8_t		commandsRegistry[REGISTERS_AMOUNT_N];



#define		MASK_ZERO		0x01
#define		MASK_ONE		0x02
#define		MASK_TWO		0x04
#define		MASK_THREE		0x08
#define		MASK_FOUR		0x10
#define		MASK_FIVE		0x20
#define		MASK_SIX		0x40
#define		MASK_SEVEN		0x80

#define		READ_OPERATION	10
#define		WRITE_OPERATION	20


/* ===== MCU STATUS =====

Address		bit values
 --- status --- 0x0A ---
7			Run State: 1 � Application ; 0 Bootloader
6			Reserved
5			Reserved
4			Reset Status:  1 - Module Reset; 0 � Reset Cleared;  DCM writes to clear
3			IRQ Status: 1 � CSIM IRQ Asserted; 0 � IRQ cleared; DCM writes to clear
2			Key Status Changed: 1 � Changed; 0 No Change; DCM writes to clear
1			Long Key Status Changed: 1 � Changed; 0 No Change; DCM writes to clear
0			Swipe Pad Status Changed: 1 � Changed; 0 No Change; DCM writes to clear
 ---control --- 0x0B
7			Reserved
6			Reserved
5			Reserved
4			Clear keypad and swipe status
3			Enter Bootloader (if present)
2			Enter test mode
1			Disable Key scans
0			Reset Registers (software reset)
*/

uint8_t   moduleStatusRegistry[12];


/* ===== SENSORS  REGISTRY =====
--- keypad 1 : 0x0C ---
7			Reserved
6			Reserved
5			Quick Clean Key
4			Stop/Resume Key
3			Speed 4 Key
2			Speed 3 Key
1			Speed 2 Key
0			Speed 1 Key
--- keypad 2 : 0x0D ---
7			Reserved
6			Reserved
5			Reserved
4			Reserved
3			Reserved
2			Right Arrow Key
1			Left Arrow Key
0			Menu Key
--- wheel : 0x0E---
7			Right Arrow Key     ----------> ? why?
6			Right Swipe Detected  ------------> clockwise?
5			Left Swipe  Detected  ------------> counterclockwise?
4			Swipe Rate  Bit 4
3			Swipe Rate  Bit 3
2			Swipe Rate  Bit 2
1			Swipe Rate  Bit 1
0			Swipe Rate  Bit 0
--- wheel step size --- 0x0F ---
7
6
5
4
3
2
1
0
--- LP keypad 1 : 0x10 ---
7			Reserved
6			Reserved
5			Quick Clean Key
4			Stop/Resume Key
3			Speed 4 Key
2			Speed 3 Key
1			Speed 2 Key
0			Speed 1 Key
--- LP keypad 2 :0x11 ---
7			Reserved
6			Reserved
5			Reserved
4			Reserved
3			Reserved
2			Right Arrow Key
1			Left Arrow Key
0			Menu Key
--- LP time period --- 0x12
7
6
5
4
3
2
1
0
--- swipes since last reading  --- 0x13
7
6
5
4
3
2
1
0
*/
uint8_t  sensorsRegistry[8];


/* ===== OUTPUT REGISTRY =====
--- LED register --- 0x14
7			LED Check System
6			LED Timers Active
5			LED Quick Clean Key
4			LED Stop/Resume Key
3			LED Speed 4 Key
2			LED Speed 3 Key
1			LED Speed 2 Key
0			LED Speed 1 Key
--- LED blinking register --- 0x15 ---
7			LED Check System
6			LED Timers Active
5			LED Quick Clean Key
4			LED Stop/Resume Key
3			LED Speed 4 Key
2			LED Speed 3 Key
1			LED Speed 2 Key
0			LED Speed 1 Key
--- LED blink rate register  --- 0x16 ---
7
6
5
4
3
2
1
0

*/
uint8_t  outputRegistry[4];



uint8_t  packetProtocolRegistry_127;


/* ===== SEND DATA =====
4		LSB
3		second byte
2		third  byte
1		fourth byte 
0		MSB   : for both bootloader and app --
*/
uint8_t  sendData[5];

#endif // ---  HAYWARD_H ---

// --- EOF hayward.h ---

